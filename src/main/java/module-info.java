module de.jbrell.librarian {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires org.json;
    requires java.sql;

    opens de.jbrell.librarian to javafx.fxml;
    exports de.jbrell.librarian;
    exports de.jbrell.librarian.webservice;
    opens de.jbrell.librarian.webservice to javafx.fxml;
    exports de.jbrell.librarian.gui;
    opens de.jbrell.librarian.gui to javafx.fxml;

    exports de.jbrell.librarian.models;
    exports de.jbrell.librarian.util;
}