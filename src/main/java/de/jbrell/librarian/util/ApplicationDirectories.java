package de.jbrell.librarian.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 05.12.21
 * <p>
 * source:
 * https://stackoverflow.com/questions/35388882/find-place-for-dedicated-application-folder
 */
public class ApplicationDirectories {
    // static fields
    public static final Path CONFIG_HOME;
    public static final Path DATA_HOME;
    public static final Path CACHE_HOME;

    static {
        String os = System.getProperty("os.name");
        String home = System.getProperty("user.home");

        if (os.contains("Mac")) {
            CONFIG_HOME = Paths.get(home, "Library", "Application Support");
            DATA_HOME = CONFIG_HOME;
            CACHE_HOME = CONFIG_HOME;
        } else if (os.contains("Windows")) {
            String version = System.getProperty("os.version");
            if (version.startsWith("5.")) {
                CONFIG_HOME = getFromEnv(
                        "APPDATA",
                        false,
                        Paths.get(home, "Application Data")
                );
                DATA_HOME = CONFIG_HOME;
                CACHE_HOME = Paths.get(home, "Local Settings", "Application Data");
            } else {
                CONFIG_HOME = getFromEnv(
                        "APPDATA",
                        false,
                        Paths.get(home, "AppData", "Roaming")
                );
                DATA_HOME = CONFIG_HOME;
                CACHE_HOME = getFromEnv(
                        "LOCALAPPDATA",
                        false,
                        Paths.get(home, "AppData", "Local")
                );
            }
        } else {
            CONFIG_HOME = getFromEnv(
                    "XDG_CONFIG_HOME",
                    true,
                    Paths.get(home, ".config")
            );
            DATA_HOME = getFromEnv(
                    "XDG_DATA_HOME",
                    true,
                    Paths.get(home, ".local", "share")
            );
            CACHE_HOME = getFromEnv(
                    "XDG_CACHE_HOME",
                    true,
                    Paths.get(home, ".cache")
            );
        }
    }

    // static private helper
    /**
     * Retrieves a path from an environment variable, substituting a default
     * if the value is absent or invalid.
     *
     * @param envVar         name of environment variable to read
     * @param mustBeAbsolute whether enviroment variable's value should be
     *                       considered invalid if it's not an absolute path
     * @param defaultPath    default to use if environment variable is absent
     *                       or invalid
     * @return environment variable's value as a {@code Path},
     * or {@code defaultPath}
     */
    private static Path getFromEnv(String envVar, boolean mustBeAbsolute, Path defaultPath) {
        Path dir;
        String envDir = System.getenv(envVar);
        if (envDir == null || envDir.isEmpty()) {
            dir = defaultPath;
            System.out.printf(
                    "\"%s\" not defined in environment, falling back on \"%s\".",
                    envVar, dir
            );
        } else {
            dir = Paths.get(envDir);
            if (mustBeAbsolute && !dir.isAbsolute()) {
                dir = defaultPath;
                System.out.printf(
                        "\"%s\" is not an absolute path, falling back on \"%s\".",
                        envVar, dir
                );
            }
        }
        return dir;
    }

    // instance fields
    private final String appName;
    private final Path configDir;
    private final Path dataDir;
    private final Path cacheDir;

    // getter
    /**
     * Returns directory where the native system expects an application
     * to store configuration files for the current user.  No attempt is made
     * to create the directory, and no checks are done to see if it exists.
     */
    public Path getConfigDir() {
        return configDir;
    }

    /**
     * Returns directory where the native system expects an application
     * to store configuration files for the current user.  No attempt is made
     * to create the directory, and no checks are done to see if it exists.
     */
    public Path getDataDir() {
        return dataDir;
    }

    /**
     * Returns directory where the native system expects an application
     * to store configuration files for the current user.  No attempt is made
     * to create the directory, and no checks are done to see if it exists.
     */
    public Path getCacheDir() {
        return cacheDir;
    }

    // constructor
    public ApplicationDirectories(String appName) {
        this.appName = appName;
        configDir = CONFIG_HOME.resolve(appName);
        dataDir = DATA_HOME.resolve(appName);
        cacheDir = CACHE_HOME.resolve(appName);
    }

    // private methods
    private boolean createDirectories(Path path) {
        try {
            return path == Files.createDirectories(path);
        } catch (IOException e) {
            System.out.printf("Could not create directory \"%s\".\n", path);
        }
        return false;
    }

    // public methods
    public boolean createConfigDir() {
        return createDirectories(configDir);
    }

    public boolean createDataDir() {
        return createDirectories(dataDir);
    }

    public boolean createCacheDir() {
        return createDirectories(cacheDir);
    }

}
