package de.jbrell.librarian.util;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 01.12.21
 */
public class JSONTools {
    // constructor
    private JSONTools() { }

    // public methods
    public static void updateJsonObject(JSONObject target, JSONObject... sources) {
        for (JSONObject source : sources) {
            for (String key : source.keySet()) {
                target.put(key, source.get(key));
            }
        }
    }

}
