package de.jbrell.librarian.util;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 03.12.21
 */
public class JavaFXTools {
    // constructor
    private JavaFXTools(){}

    // public methods
    /**
     * source:
     *      https://community.oracle.com/tech/developers/discussion/2319231/making-labels-and-other-controls-with-text-selectable-for-copy-paste
     * @return the passed in label made selectable.
     * */
    private Label makeSelectable(final Label label) {
        StackPane textStack = new StackPane();
        final TextArea textArea = new TextArea();
        Text labelText = (Text) label.lookup(".text");
        textArea.textProperty().bind(labelText.textProperty());
        textArea.setWrapText(true);
        textArea.setEditable(false);
        textArea.setStyle(
                """
                        -fx-background-color: transparent;
                        -fx-background-insets: 0;
                        -fx-background-radius: 0;
                        -fx-padding: 0;"""
        );
        // the invisible label is a hack to get the textField to size like a label (maybe it is not really necessary)
        Label invisibleLabel = new Label();
        invisibleLabel.setWrapText(true);
        invisibleLabel.textProperty().bind(label.textProperty());
        invisibleLabel.setVisible(false);
        textStack.getChildren().addAll(invisibleLabel, textArea);
        textArea.textProperty().bind(label.textProperty());
        label.setGraphic(textStack);
        label.setAlignment(Pos.TOP_LEFT);
        labelText.setVisible(false);
        labelText.setManaged(false);
        return label;
    }
}
