package de.jbrell.librarian.util;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 01.12.21
 */
public enum QueryCondition {
    WHERE, LIKE, HAVING
}
