package de.jbrell.librarian;

import de.jbrell.librarian.daos.AuthorDaoSQLite;
import de.jbrell.librarian.daos.AuthorDaoWebAPI;
import de.jbrell.librarian.daos.BookDaoWebAPI;
import de.jbrell.librarian.db.SQLiteConnector;
import de.jbrell.librarian.models.Author;
import de.jbrell.librarian.models.Book;
import de.jbrell.librarian.models.BookExcerpt;
import de.jbrell.librarian.models.Link;
import de.jbrell.librarian.util.ApplicationDirectories;
import de.jbrell.librarian.webservice.HttpRequests;
import de.jbrell.librarian.webservice.Isbn;
import de.jbrell.librarian.webservice.OpenLibraryService;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 03.12.21
 */
public class Tests {
    // static fields
    public static Author author = new Author();
    static {
        author.setName("J. K. Rowling");
        author.setTitle("OBE");
        author.setBirthDate(LocalDate.of(1965, 7, 31));
        author.setPersonalName("J. K. Rowling");
        author.setAlternateNames(Arrays.stream(new String[]{
                "Joanne Rowling",
                "Joanne K. Rowling",
                "Jo Murray",
                "J K Rowling",
                "Kennilworthy Whisp",
                "JK Rowling",
                "Robert Galbraith",
                "Robert Galbraith (J.K. Rowling)",
                "Robert Galbraith (pseud. J.K. Rowling)",
                "Robert Galibraith"
        }).toList());
        author.setBio("Joanne \"Jo\" Murray, OBE (née Rowling), better known under the pen name J. K. Rowling, is a British author best known as the creator of the Harry Potter fantasy series, the idea for which was conceived whilst on a train trip from Manchester to London in 1990. The Potter books have gained worldwide attention, won multiple awards, sold more than 400 million copies, and been the basis for a popular series of films.");
        try {
            List<Link> linkArrayList = new ArrayList<>(1);
            linkArrayList.add(
                    new Link(
                            "Official Site",
                            new URL("http://www.jkrowling.com/")
                    )
            );
            author.setLinks(linkArrayList);
        } catch (
                MalformedURLException e) {
            e.printStackTrace();
        }
    }

    // static methods
    private static void testAuthorDaoWebAPI() {
        AuthorDaoWebAPI authorDaoWebAPI = new AuthorDaoWebAPI();
        List<Author> authorList = authorDaoWebAPI.read("Pratchett");
        JSONArray jsonArray = new JSONArray();
        for (Author author : authorList) {
            jsonArray.put(author.toJson());
        }
        System.out.println(jsonArray.toString(4));
//        System.out.println();
//        System.out.println(authorDaoWebAPI.cachePreviewed);
//        System.out.println(authorDaoWebAPI.cacheDetailed);
        System.out.println();

        jsonArray.clear();
        for (Author author : authorList) {
            authorDaoWebAPI.update(author);
            jsonArray.put(author.toJson());
        }
        System.out.println(jsonArray.toString(4));
//        System.out.println();
//        System.out.println(authorDaoWebAPI.cachePreviewed);
//        System.out.println(authorDaoWebAPI.cacheDetailed);
        System.out.println();
    }

    private static void testApplicationDirectories() {
        final String APPNAME = "librarian";
        // generic
        System.out.printf(
                "CACHE_HOME → %s\n",
                ApplicationDirectories.CACHE_HOME
        );
        System.out.printf(
                "CONFIG_HOME → %s\n",
                ApplicationDirectories.CONFIG_HOME
        );
        System.out.printf(
                "DATA_HOME → %s\n",
                ApplicationDirectories.DATA_HOME
        );
        // application specific
        ApplicationDirectories appDirs = new ApplicationDirectories("librarian");
        // get
        System.out.printf(
                "getCacheDir() → %s\n",
                appDirs.getCacheDir()
        );
        System.out.printf(
                "getConfigDir() → %s\n",
                appDirs.getCacheDir()
        );
        System.out.printf(
                "getDataDir() → %s\n",
                appDirs.getCacheDir()
        );
        // create
        System.out.printf(
                "createCacheDir() → %s\n",
                appDirs.createCacheDir()
        );
        System.out.printf(
                "createConfigDir() → %s\n",
                appDirs.createConfigDir()
        );
        System.out.printf(
                "createDataDir() → %s\n",
                appDirs.createDataDir()
        );
    }

    private static void testSQLiteConnector() {
        SQLiteConnector sqLiteConnector = new SQLiteConnector();
        System.out.printf(
                "sqLiteConnector.getFilepath() → \"%s\"\n",
                sqLiteConnector.getFilepath()
        );
    }

    private static void testAuthorDaoSQLite() {
        AuthorDaoSQLite authorDaoSQLite = new AuthorDaoSQLite();
        authorDaoSQLite.createTable();
    }

    private static void testQueryBookByISBN() {
        Isbn isbn = new Isbn("978-3-499-23546-7");
        JSONObject jsonObject = OpenLibraryService.getBookByISBN(isbn);
        System.out.println(jsonObject.toString(4));
    }

    private static void testQueryBookByKey() {
        JSONObject jsonObject = OpenLibraryService.getBookByOpenLibraryKey("OL27094767M");
        System.out.println(jsonObject.toString(4));
    }

    private static void testGetAuthorKeyFromUrl() {
        String url = "https://openlibrary.org/authors/OL32223A/Tom_Robbins";
        String openLibraryKey = BookDaoWebAPI.getAuthorKeyFromUrl(url);
        if (openLibraryKey.isEmpty()) {
            System.out.println("Nada!");
        } else {
            System.out.println(openLibraryKey);
        }
    }

    private static void testGetBookByOpenLibraryKey(){
        BookDaoWebAPI bookDaoWebAPI = new BookDaoWebAPI();
        Book book = bookDaoWebAPI.readOne("OL27094767M");
        System.out.println(book.toJson().toString(4));
    }

    private static void testGetBookByISBN13(){
        BookDaoWebAPI bookDaoWebAPI = new BookDaoWebAPI();
        Book book = bookDaoWebAPI.readOne(new Isbn(" 978-3-499-23546-7"));
        System.out.println(book.toJson().toString(4));
    }

    private static void testGetBookByISBN10(){
        BookDaoWebAPI bookDaoWebAPI = new BookDaoWebAPI();
        Book book = bookDaoWebAPI.readOne(new Isbn("3-499-23546-3"));
        System.out.println(book.toJson().toString(4));
    }

    private static void testEncodeQuery(){
        String encodedQuery;
//        encodedQuery = HttpRequests.encodeQuery("Völker dieser Welt, relaxt!");
//        encodedQuery = HttpRequests.encodeQuery("Discworld (imaginary place), fiction");
        encodedQuery = HttpRequests.encodeQuery("Children's fiction");
        System.out.println(encodedQuery);
    }

    public static void main(String[] args) {
//        JSONObject jsonObject = OpenLibraryService.getAuthorsByQuery("J. K. Rowling");
//        System.out.println(jsonObject.toString(4));
//        testAuthorDaoWebAPI();
//        testApplicationDirectories();
//        testSQLiteConnector();
//        testAuthorDaoSQLite();
//        testEncodeQuery();
//        testQueryBookByISBN();
//        testQueryBookByKey();
//        testGetAuthorKeyFromUrl();
//        testGetBookByOpenLibraryKey();
//        testGetBookByISBN13();
        testGetBookByISBN10();
    }
}
