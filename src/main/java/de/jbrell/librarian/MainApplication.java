package de.jbrell.librarian;

import de.jbrell.librarian.daos.AuthorDaoWebAPI;
import de.jbrell.librarian.gui.MainController;
import de.jbrell.librarian.models.Author;
import de.jbrell.librarian.util.ApplicationDirectories;
import de.jbrell.librarian.webservice.OpenLibraryService;
import javafx.application.Application;
import javafx.stage.Stage;
import org.json.JSONObject;

public class MainApplication extends Application {

    // static fields
    public static final String APP_TITLE = "librarian";
    public static final ApplicationDirectories APP_DIRS = new ApplicationDirectories(APP_TITLE);
    static {
        APP_DIRS.createCacheDir();
    }

    // lifecycle methods
    @Override
    public void start(Stage primaryStage) {
        new MainController(this, primaryStage);
    }

    public static void main(String[] args) {
        launch(args);
    }

    // other methods
    public void openBrowser(String url) {
        getHostServices().showDocument(url);
    }
}