package de.jbrell.librarian.models;

import org.json.JSONObject;

import java.net.URL;
import java.util.List;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 07.12.21
 */
public class Link {
    // instance fields
    private String title;
    private URL url;

    // getter & setter
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    // constructor
    public Link() { }

    public Link(URL url) {
        setUrl(url);
    }

    public Link(String title, URL url) {
        this(url);
        setTitle(title);
    }

    // public methods
    @Override
    public String toString() {
        return String.format(
                "%s{title=%s, url=%s}",
                this.getClass().getSimpleName(),
                title, url.toString()
        );
    }

    public JSONObject toJson() {
        // (create)
        JSONObject jsonObject = new JSONObject();
        // title
        if (title != null && !title.isEmpty()) {
            jsonObject.put("title", title);
        }
        // url
        if (url != null) {
            jsonObject.put("url", url.toString());
        }
        // (return)
        return jsonObject;
    }
}
