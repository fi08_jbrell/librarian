package de.jbrell.librarian.models;

import org.json.JSONObject;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 07.12.21
 */
public class BookIdentifiers {

    // instance fields
    private String isbn10;
    private String isbn13;
    private String amazon;
    private String goodreads;
    private String google;
    private String lccn;
    private String librarything;
    private String oclc;
    private String openlibrary;
    private String projectGutenberg;

    // getter & setter
    public String getIsbn10() {
        return isbn10;
    }

    public void setIsbn10(String isbn10) {
        this.isbn10 = isbn10;
    }

    public String getIsbn13() {
        return isbn13;
    }

    public void setIsbn13(String isbn13) {
        this.isbn13 = isbn13;
    }

    public String getAmazon() {
        return amazon;
    }

    public void setAmazon(String amazon) {
        this.amazon = amazon;
    }

    public String getGoodreads() {
        return goodreads;
    }

    public void setGoodreads(String goodreads) {
        this.goodreads = goodreads;
    }

    public String getGoogle() {
        return google;
    }

    public void setGoogle(String google) {
        this.google = google;
    }

    public String getLccn() {
        return lccn;
    }

    public void setLccn(String lccn) {
        this.lccn = lccn;
    }

    public String getLibrarything() {
        return librarything;
    }

    public void setLibrarything(String librarything) {
        this.librarything = librarything;
    }

    public String getOclc() {
        return oclc;
    }

    public void setOclc(String oclc) {
        this.oclc = oclc;
    }

    public String getOpenlibrary() {
        return openlibrary;
    }

    public void setOpenlibrary(String openlibrary) {
        this.openlibrary = openlibrary;
    }

    public String getProjectGutenberg() {
        return projectGutenberg;
    }

    public void setProjectGutenberg(String projectGutenberg) {
        this.projectGutenberg = projectGutenberg;
    }

    // constructor
    public BookIdentifiers() { }

    // public methods
    @Override
    public String toString() {
        return String.format(
                "%s{isbn10=%s, isbn13=%s, ..., openLibraryKey=%s}",
                this.getClass().getSimpleName(),
                isbn10, isbn13, openlibrary
        );
    }

    public JSONObject toJson() {
        // (create)
        JSONObject jsonObject = new JSONObject();
        // isbn10
        if (isbn10 != null && !isbn10.isEmpty()) {
            jsonObject.put("isbn10", isbn10);
        }
        // isbn13
        if (isbn13 != null && !isbn13.isEmpty()) {
            jsonObject.put("isbn13", isbn13);
        }
        // amazon
        if (amazon != null && !amazon.isEmpty()) {
            jsonObject.put("amazon", amazon);
        }
        // goodreads
        if (goodreads != null && !goodreads.isEmpty()) {
            jsonObject.put("goodreads", goodreads);
        }
        // google
        if (google != null && !google.isEmpty()) {
            jsonObject.put("google", google);
        }
        // lccn
        if (lccn != null && !lccn.isEmpty()) {
            jsonObject.put("lccn", lccn);
        }
        // librarything
        if (librarything != null && !librarything.isEmpty()) {
            jsonObject.put("librarything", librarything);
        }
        // oclc
        if (oclc != null && !oclc.isEmpty()) {
            jsonObject.put("oclc", oclc);
        }
        // openlibrary
        if (openlibrary != null && !openlibrary.isEmpty()) {
            jsonObject.put("openlibrary", openlibrary);
        }
        // projectGutenberg
        if (projectGutenberg != null && !projectGutenberg.isEmpty()) {
            jsonObject.put("projectGutenberg", projectGutenberg);
        }
        // (return)
        return jsonObject;
    }
}
