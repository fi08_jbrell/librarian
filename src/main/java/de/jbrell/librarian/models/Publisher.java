package de.jbrell.librarian.models;

import org.json.JSONObject;

import java.net.URL;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 07.12.21
 */
public class Publisher {

    // instance fields
    private String name;

    // getter & setter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // constructor
    public Publisher() { }

    public Publisher(String name) {
        setName(name);
    }

    // public methods
    @Override
    public String toString() {
        return String.format(
                "%s{name=%s}",
                this.getClass().getSimpleName(),
                name
        );
    }

    public JSONObject toJson() {
        // (create)
        JSONObject jsonObject = new JSONObject();
        // name
        if (name != null && !name.isEmpty()) {
            jsonObject.put("name", name);
        }
        // (return)
        return jsonObject;
    }
}
