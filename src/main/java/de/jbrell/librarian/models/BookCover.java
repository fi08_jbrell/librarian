package de.jbrell.librarian.models;

import javafx.scene.image.Image;
import org.json.JSONObject;

import java.net.URL;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 07.12.21
 */
public class BookCover {
    // instance fields
    private URL urlSmall;
    private URL urlMedium;
    private URL urlLarge;
    private Image imageSmall;
    private Image imageMedium;
    private Image imageLarge;

    // getter & setter
    public URL getUrlSmall() {
        return urlSmall;
    }

    public void setUrlSmall(URL urlSmall) {
        this.urlSmall = urlSmall;
    }

    public URL getUrlMedium() {
        return urlMedium;
    }

    public void setUrlMedium(URL urlMedium) {
        this.urlMedium = urlMedium;
    }

    public URL getUrlLarge() {
        return urlLarge;
    }

    public void setUrlLarge(URL urlLarge) {
        this.urlLarge = urlLarge;
    }

    public Image getImageSmall() {
        return imageSmall;
    }

    public void setImageSmall(Image imageSmall) {
        this.imageSmall = imageSmall;
    }

    public Image getImageMedium() {
        return imageMedium;
    }

    public void setImageMedium(Image imageMedium) {
        this.imageMedium = imageMedium;
    }

    public Image getImageLarge() {
        return imageLarge;
    }

    public void setImageLarge(Image imageLarge) {
        this.imageLarge = imageLarge;
    }

    // constructor
    public BookCover() { }

    // public methods
    @Override
    public String toString() {
        return String.format(
                "%s{small=%s, medium=%s, large=%s}",
                this.getClass().getSimpleName(),
                urlSmall, urlMedium, urlLarge
        );
    }

    public JSONObject toJson() {
        // (create)
        JSONObject jsonObject = new JSONObject();
        // small
        if (urlSmall != null) {
            jsonObject.put("small", urlSmall.toString());
        }
        // medium
        if (urlSmall != null) {
            jsonObject.put("medium", urlMedium.toString());
        }
        // small
        if (urlLarge != null) {
            jsonObject.put("large", urlLarge.toString());
        }
        // (return)
        return jsonObject;
    }
}

