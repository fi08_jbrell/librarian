package de.jbrell.librarian.models;

import org.json.JSONObject;

import java.net.URL;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 07.12.21
 */
public class BookExcerpt {
    // instance fields
    private String comment;
    private String text;

    // getter & setter
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    // constructor
    public BookExcerpt() { }

    public BookExcerpt(String text) {
        setText(text);
    }

    public BookExcerpt(String comment, String text) {
        this(text);
        setComment(comment);
    }

    // public methods
    @Override
    public String toString() {
        return String.format(
                "%s{comment=%s, text=%s…}",
                this.getClass().getSimpleName(),
                comment, text.substring(0, 12)
        );
    }

    public JSONObject toJson() {
        // (create)
        JSONObject jsonObject = new JSONObject();
        // title
        if (comment != null && !comment.isEmpty()) {
            jsonObject.put("comment", comment);
        }
        // text
        if (text != null && !text.isEmpty()) {
            jsonObject.put("text", text);
        }
        // (return)
        return jsonObject;
    }
}
