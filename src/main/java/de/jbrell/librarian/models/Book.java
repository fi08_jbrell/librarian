package de.jbrell.librarian.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 07.12.21
 */
public class Book {

    // static fields
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(
            "d MMMM yyyy", Locale.ENGLISH
    );

    // instance fields
    private String title;
    private String subtitle;
    private List<Author> authors;
    private BookIdentifiers identifiers;
    private List<Link> subjects;
    private List<Publisher> publishers;
    private List<String> publishPlaces;
    private LocalDate publishDate;
    private Integer publishYear;
    private String publishDateString;
    private List<BookExcerpt> excerpts;
    private List<Link> links;
    private BookCover cover;
    private String pagination;
    private Integer pageCount;
    private String notes;
    private String byStatement;

    // getter & setter
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public BookIdentifiers getIdentifiers() {
        return identifiers;
    }

    public void setIdentifiers(BookIdentifiers identifiers) {
        this.identifiers = identifiers;
    }

    public List<Link> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Link> subjects) {
        this.subjects = subjects;
    }

    public List<Publisher> getPublishers() {
        return publishers;
    }

    public void setPublishers(List<Publisher> publishers) {
        this.publishers = publishers;
    }

    public List<String> getPublishPlaces() {
        return publishPlaces;
    }

    public void setPublishPlaces(List<String> publishPlaces) {
        this.publishPlaces = publishPlaces;
    }

    public LocalDate getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(LocalDate publishDate) {
        this.publishDate = publishDate;
    }

    public Integer getPublishYear() {
        return publishYear;
    }

    public void setPublishYear(Integer publishYear) {
        this.publishYear = publishYear;
    }

    public String getPublishDateString() {
        return publishDateString;
    }

    public void setPublishDateString(String publishDateString) {
        this.publishDateString = publishDateString;
    }

    public List<BookExcerpt> getExcerpts() {
        return excerpts;
    }

    public void setExcerpts(List<BookExcerpt> excerpts) {
        this.excerpts = excerpts;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    public BookCover getCover() {
        return cover;
    }

    public void setCover(BookCover cover) {
        this.cover = cover;
    }

    public String getPagination() {
        return pagination;
    }

    public void setPagination(String pagination) {
        this.pagination = pagination;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getByStatement() {
        return byStatement;
    }

    public void setByStatement(String byStatement) {
        this.byStatement = byStatement;
    }

    // constructor
    public Book() { }

    // public methods
    @Override
    public String toString() {
        return String.format(
                "%s{title=%s, ...}",
                this.getClass().getSimpleName(), title
        );
    }

    public JSONObject toJson() {
        // (create)
        JSONObject jsonObject = new JSONObject();
        // title
        if (title != null && !title.isEmpty()) {
            jsonObject.put("title", title);
        }
        // subtitle
        if (subtitle != null && !subtitle.isEmpty()) {
            jsonObject.put("subtitle", subtitle);
        }
        // authors
        if (authors != null && authors.size() > 0) {
            jsonObject.put(
                    "authors",
                    new JSONArray(
                            authors.stream()
                                    .map(Author::toJson)
                                    .collect(Collectors.toList())
                    )
            );
        }
        // identifiers
        if (identifiers != null) {
            jsonObject.put("identifiers", identifiers.toJson());
        }
        // subjects
        if (subjects != null && subjects.size() > 0) {
            jsonObject.put(
                    "subjects",
                    new JSONArray(
                            subjects.stream()
                                    .map(Link::toJson)
                                    .collect(Collectors.toList())
                    )
            );
        }
        // publishers
        if (publishers != null && publishers.size() > 0) {
            jsonObject.put(
                    "publishers",
                    new JSONArray(
                            publishers.stream()
                                    .map(Publisher::toJson)
                                    .collect(Collectors.toList())
                    )
            );
        }
        // publishPlaces
        if (publishPlaces != null && publishPlaces.size() > 0) {
            jsonObject.put(
                    "publishPlaces",
                    new JSONArray(publishPlaces)
            );
        }
        // publishDate, publishYear, publishDateString
        if (publishDate != null) {
            jsonObject.put(
                    "publishDate",
                    publishDate.format(dateTimeFormatter)
            );
        } else if (publishYear != null) {
            jsonObject.put(
                    "publishYear",
                    String.valueOf(publishYear)
            );
        } else if (publishDateString != null && !publishDateString.isEmpty()) {
            jsonObject.put("publishDateString", publishDateString);
        }
        // excerpts
        if (excerpts != null && excerpts.size() > 0) {
            jsonObject.put(
                    "excerpts",
                    new JSONArray(
                            excerpts.stream()
                                    .map(BookExcerpt::toJson)
                                    .collect(Collectors.toList())
                    )
            );
        }
        // links
        if (links != null && links.size() > 0) {
            jsonObject.put(
                    "links",
                    new JSONArray(
                            links.stream()
                                    .map(Link::toJson)
                                    .collect(Collectors.toList())
                    )
            );
        }
        // cover
        if (cover != null) {
            jsonObject.put("cover", cover.toJson());
        }
        // pagination
        if (pagination != null && !pagination.isEmpty()) {
            jsonObject.put("pagination", pagination);
        }
        // pageCount
        if (pageCount != null) {
            jsonObject.put("pageCount", pageCount);
        }
        // notes
        if (notes != null && !notes.isEmpty()) {
            jsonObject.put("notes", notes);
        }
        // byStatement
        if (byStatement != null && !byStatement.isEmpty()) {
            jsonObject.put("byStatement", byStatement);
        }
        // (return)
        return jsonObject;
    }
}
