package de.jbrell.librarian.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 30.11.21
 */
public class Author {

    // static fields
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(
            "d MMMM yyyy", Locale.ENGLISH
    );

    // instance fields
    private String name;
    private String personalName;
    private List<String> alternateNames;
    private String title;
    private LocalDate birthDate;
    private Integer birthYear;
    private String birthDateString;
    private Integer deathYear;
    private LocalDate deathDate;
    private String deathDateString;
    private String bio;
    private Integer workCount;
    private String topWork;
    private List<String> topSubjects;
    private List<Link> links;
    private String openLibraryKey;

    // getter & setter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPersonalName() {
        return personalName;
    }

    public void setPersonalName(String personalName) {
        this.personalName = personalName;
    }

    public List<String> getAlternateNames() {
        return alternateNames;
    }

    public void setAlternateNames(List<String> alternateNames) {
        this.alternateNames = alternateNames;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(Integer birthYear) {
        this.birthYear = birthYear;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthDateString() {
        return birthDateString;
    }

    public void setBirthDateString(String birthDateString) {
        this.birthDateString = birthDateString;
    }

    public Integer getDeathYear() {
        return deathYear;
    }

    public void setDeathYear(Integer deathYear) {
        this.deathYear = deathYear;
    }

    public LocalDate getDeathDate() {
        return deathDate;
    }

    public void setDeathDate(LocalDate deathDate) {
        this.deathDate = deathDate;
    }

    public String getDeathDateString() {
        return deathDateString;
    }

    public void setDeathDateString(String deathDateString) {
        this.deathDateString = deathDateString;
    }

    public String getBio() {
        return bio;
    }

    public Integer getWorkCount() {
        return workCount;
    }

    public void setWorkCount(Integer workCount) {
        this.workCount = workCount;
    }

    public String getTopWork() {
        return topWork;
    }

    public void setTopWork(String topWork) {
        this.topWork = topWork;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public List<String> getTopSubjects() {
        return topSubjects;
    }

    public void setTopSubjects(List<String> topSubjects) {
        this.topSubjects = topSubjects;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    public String getOpenLibraryKey() {
        return openLibraryKey;
    }

    public void setOpenLibraryKey(String openLibraryKey) {
        this.openLibraryKey = openLibraryKey;
    }

    // constructor
    public Author() { }

    // public methods
    @Override
    public String toString() {
        return String.format(
                "%s{name=%s, ..., openLibraryKey=%s}",
                this.getClass().getSimpleName(),
                name, openLibraryKey
        );
    }

    public JSONObject toJson() {
        // (create)
        JSONObject jsonObject = new JSONObject();
        // name
        if (name != null && !name.isEmpty()) {
            jsonObject.put("name", name);
        }
        // personalName
        if (personalName != null && !personalName.isEmpty()) {
            jsonObject.put("personalName", personalName);
        }
        // alternateNames
        if (alternateNames != null && alternateNames.size() > 0) {
            jsonObject.put(
                    "alternateNames",
                    new JSONArray(alternateNames)
            );
        }
        // title
        if (title != null && !title.isEmpty()) {
            jsonObject.put("title", title);
        }
        // birthDate, birthYear, birthDateString
        if (birthDate != null) {
            jsonObject.put(
                    "birthDate",
                    birthDate.format(dateTimeFormatter)
            );
        } else if (birthYear != null) {
            jsonObject.put(
                    "birthYear",
                    String.valueOf(birthYear)
            );
        } else if (birthDateString != null && !birthDateString.isEmpty()) {
            jsonObject.put("birthDateString", birthDateString);
        }
        // deathDate, deathYear, deathDateString
        if (deathDate != null) {
            jsonObject.put(
                    "deathDate",
                    deathDate.format(dateTimeFormatter)
            );
        } else if (deathYear != null) {
            jsonObject.put(
                    "deathYear",
                    String.valueOf(deathYear)
            );
        } else if (deathDateString != null && !deathDateString.isEmpty()) {
            jsonObject.put("deathDateString", deathDateString);
        }
        // bio
        if (bio != null && !bio.isEmpty()) {
            jsonObject.put("bio", bio);
        }
        // bio
        if (bio != null && !bio.isEmpty()) {
            jsonObject.put("bio", bio);
        }
        // workCount
        if (workCount != null) {
            jsonObject.put(
                    "workCount",
                    String.valueOf(workCount)
            );
        }
        // topWork
        if (topWork != null && !topWork.isEmpty()) {
            jsonObject.put("topWork", topWork);
        }
        // topSubjects
        if (topSubjects != null && topSubjects.size() > 0) {
            jsonObject.put(
                    "topSubjects",
                    new JSONArray(topSubjects)
            );
        }
        // links
        if (links != null && links.size() > 0) {
            jsonObject.put(
                    "links",
                    new JSONArray(
                            links.stream()
                                    .map(Link::toJson)
                                    .collect(Collectors.toList())
                    )
            );
        }
        // openLibraryKey
        if (openLibraryKey != null && !openLibraryKey.isEmpty()) {
            jsonObject.put("openLibraryKey", openLibraryKey);
        }
        // (return)
        return jsonObject;
    }
}
