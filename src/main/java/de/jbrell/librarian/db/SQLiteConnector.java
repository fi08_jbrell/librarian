package de.jbrell.librarian.db;

import de.jbrell.librarian.MainApplication;

import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 05.12.21
 */
public class SQLiteConnector implements AutoCloseable {
    // static fields
    private static final String TEMPLATE_CONNECTION_STRING = "jdbc:sqlite:%s";
    private static final String DEFAULT_DB_FILENAME = MainApplication.APP_TITLE + ".sqlite";

    // instance fields
    private String filepath;
    private Connection connection;

    // getter & setter
    public String getFilepath() {
        return filepath;
    }
//    // TODO: Implement DB-Backup and make public setter to set custom db file location.
//    public void setFilepath(String filepath) {
//        this.filepath = filepath;
//    }

    private void setDefaultFilepath() {
        Path cacheDir = MainApplication.APP_DIRS.getCacheDir();
        if (Files.isDirectory(cacheDir)) {
            filepath = cacheDir.resolve(DEFAULT_DB_FILENAME).toString();
        } else {
            filepath = ":memory:";
        }
    }

    public String getConnectionString() {
        return String.format(TEMPLATE_CONNECTION_STRING, filepath);
    }

    public Connection getConnection() throws SQLException {
        if (connection == null) {
            connection = DriverManager.getConnection(
                    getConnectionString()
            );
        }
        return connection;
    }

    // constructor
    public SQLiteConnector() {
        setDefaultFilepath();
    }

    // public methods
    @Override
    public void close() throws SQLException {
        if (connection != null) {
            connection.close();
            connection = null;
        }
    }
}
