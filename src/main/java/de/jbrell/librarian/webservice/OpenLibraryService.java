package de.jbrell.librarian.webservice;

import org.json.JSONObject;

import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 30.11.21
 */
public class OpenLibraryService {
    // static fields
    public static final String BASE_URL = "https://openlibrary.org";

    public static final DateTimeFormatter[] DateParseFormatters = {
            DateTimeFormatter.ofPattern("dd MMMM yyyy", Locale.ENGLISH),
            DateTimeFormatter.ofPattern("dd MMM yyyy", Locale.ENGLISH),
            DateTimeFormatter.ofPattern("d MMMM yyyy", Locale.ENGLISH),
            DateTimeFormatter.ofPattern("d MMM yyyy", Locale.ENGLISH)
    };

    private static final String TEMPLATE_GET_BOOK_BY_BIBKEY = (
            "https://openlibrary.org/api/books?bibkeys=%s&jscmd=data&format=json"
    );

    // constructor
    private OpenLibraryService() {}

    // private methods
    private static JSONObject getContentByKey(OpenLibraryApi openLibraryApi, OpenLibraryKey openLibraryKey) {
        assert openLibraryApi.doesRequestByKey(): String.format("%s API does not query by key", openLibraryApi.identifier);
        String request = String.format(
                "%s/%s/%s.json",
                BASE_URL,
                openLibraryApi.identifier,
                openLibraryKey.key()
        );
        return HttpRequests.getJsonObjectFromUrlRequest(request);
    }

    private static JSONObject getContentByQuery(OpenLibraryApi openLibraryApi, String query) {
        String request = String.format(
                "%s/search/%s.json?q=%s",
                BASE_URL,
                openLibraryApi.identifier,
                HttpRequests.encodeQuery(query)
        );
        return HttpRequests.getJsonObjectFromUrlRequest(request);
    }

    // public methods
    public static JSONObject getAuthorByKey(String openLibraryKey) {
        return getContentByKey(
                OpenLibraryApi.AUTHORS,
                new OpenLibraryKey(openLibraryKey)
        );
    }

    public static JSONObject getEditionByKey(String openLibraryKey) {
        return getContentByKey(
                OpenLibraryApi.EDITIONS,
                new OpenLibraryKey(openLibraryKey)
        );
    }

    public static JSONObject getAuthorsByQuery(String query) {
        return getContentByQuery(OpenLibraryApi.AUTHORS, query);
    }

/*
    public static JSONObject search(String query) {
        String response = "{}";
        try {
            String encodedQuery = URLEncoder.encode(query, StandardCharsets.UTF_8.name());
            response = WebService.getJsonFromUrl(
                    API_ROOT_URL + "search?q=" + encodedQuery + "&mode=everything"
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new JSONObject(response);
    }
*/

    /*
        BOOKS
    */
    private static JSONObject getBookByBibKey(String bibKey) {
        String request = String.format(
                TEMPLATE_GET_BOOK_BY_BIBKEY,
                bibKey
        );
        JSONObject response = HttpRequests.getJsonObjectFromUrlRequest(request);
        if (response.has(bibKey)) {
            return response.getJSONObject(bibKey);
        }
        return response;
    }

    public static JSONObject getBookByOpenLibraryKey(String openLibraryKey) {
        return getBookByBibKey(
                new OpenLibraryKey(openLibraryKey).key()
        );
    }

    public static JSONObject getBookByISBN(Isbn isbn) {
        return getBookByBibKey(
                "ISBN:" + isbn.isbn()
        );
    }
}
