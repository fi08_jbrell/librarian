package de.jbrell.librarian.webservice;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 01.12.21
 */
public record OpenLibraryKey(String key) {

    public OpenLibraryKey {
        assert (key != null || !key.isEmpty()): "OpenLibrary key must not be empty.";
    }
}
