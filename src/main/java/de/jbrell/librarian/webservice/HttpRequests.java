package de.jbrell.librarian.webservice;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

/**
 * <h1>
 * WebService
 * </h1>
 * <p>
 * HTTP Verbindung
 * https://www.baeldung.com/httpurlconnection-post
 * </p>
 *
 * @author J.Brell
 * @version 0.1.0
 * @since 28.11.21
 */
public class HttpRequests {
    // constructor
    private HttpRequests() { }

    // private methods
    private static String readResponse(HttpURLConnection connection) throws IOException {
        InputStream inputStream = connection.getInputStream();
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            String row;
            while ((row = reader.readLine()) != null) {
                stringBuilder.append(row);
            }
        }
        return stringBuilder.toString();
    }

    // public methods
    public static String encodeQuery(String query) {
        String result = "";
        try {
            result = URLEncoder.encode(query, StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            // Won't happen, because the use of StandardCharsets.UTF_8 is hardcoded here.
            assert false;
        }
        return result;
    }

    public static Optional<String> getJsonStringFromUrlRequest(String request) {
        try {
            URL url = new URL(request);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestMethod("GET");
            return Optional.of(readResponse(connection));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public static JSONObject getJsonObjectFromUrlRequest(String request) {
        Optional<String> response = HttpRequests.getJsonStringFromUrlRequest(request);
        if (response.isEmpty()) {
            return new JSONObject();
        }
        return new JSONObject(response.get());
    }

    public static JSONArray getJsonArrayFromUrlRequest(String request) {
        Optional<String> response = HttpRequests.getJsonStringFromUrlRequest(request);
        if (response.isEmpty()) {
            return new JSONArray();
        }
        String unwrappedResponse = response.get();
        if (!unwrappedResponse.startsWith("[")) {
            unwrappedResponse = String.format("[%s]", unwrappedResponse);
        }
        return new JSONArray(unwrappedResponse);
    }
}



