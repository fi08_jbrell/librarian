package de.jbrell.librarian.webservice;

import java.util.EnumSet;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 01.12.21
 */
public enum OpenLibraryApi {
    // members
    AUTHORS("authors"),
    WORKS("works"),
    EDITIONS("books"),
    ;

    // static fields
    private static final EnumSet<OpenLibraryApi> requestsByKey = EnumSet.of(
            OpenLibraryApi.AUTHORS,
            OpenLibraryApi.WORKS,
            OpenLibraryApi.EDITIONS
    );

    // instance fields
    public final String identifier;

    // constructors
    OpenLibraryApi(String identifier) {
        this.identifier = identifier;
    }

    // public methods
    public boolean doesRequestByKey() {
        return requestsByKey.contains(this);
    }
}
