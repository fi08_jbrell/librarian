package de.jbrell.librarian.webservice;

import java.util.Objects;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 08.12.21
 */
public record Isbn(String isbn) {

    public Isbn(String isbn) {
        Objects.requireNonNull(isbn);
        String normalizedIsbn = isbn.replaceAll("[ -]", "");
        int normalizedIsbnLength = normalizedIsbn.length();
        if (normalizedIsbnLength != 10 && normalizedIsbnLength != 13) {
            throw new IllegalArgumentException(
                    String.format(
                            "ISBN must contain exactly 10 or 13 digits, but got \"%s\".",
                            isbn
                    )
            );
        }
        this.isbn = normalizedIsbn;
    }

    public boolean isISBN10() {
        return (isbn.length() == 10);
    }

    public boolean isISBN13() {
        return (isbn.length() == 13);
    }

    // TODO: Write a method to rehyphenate (this will be non-trivial).
}
