package de.jbrell.librarian.gui;

import de.jbrell.librarian.MainApplication;
import de.jbrell.librarian.daos.AuthorDaoWebAPI;
import de.jbrell.librarian.daos.BookDaoWebAPI;
import de.jbrell.librarian.models.Author;
import de.jbrell.librarian.models.Book;
import de.jbrell.librarian.webservice.Isbn;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.util.List;
import java.util.Objects;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 02.12.21
 */
public class MainController {
    // static fields
    public static final String WINDOW_TITLE = "the Librarian";

    public static final int INITIAL_WIDTH = 1280;
    public static final int INITIAL_HEIGHT = 720;
    public static final int MINIMAL_WIDTH = 640;
    public static final int MINIMAL_HEIGHT = 480;

    private static final int CSS_FORMAT_INDEX = 0;
    private static final int CSS_COLOR_INDEX = 1;

    public static final String DEFAULT_FILEPATH_CSS_FORMAT = Objects.requireNonNull(
            MainController.class.getResource("format_default.css")
    ).toExternalForm();

    public static final String DEFAULT_FILEPATH_CSS_COLOR = Objects.requireNonNull(
            MainController.class.getResource("color_default.css")
//            MainController.class.getResource("color_dark.css")
    ).toExternalForm();
    // TODO: Add button for dark mode when styling is finished.

    // instance fields
    private final MainApplication app;
    private final Stage stage;
    private final Scene scene;
    private final MainView root;
    private final AuthorDaoWebAPI authorDaoWebAPI = new AuthorDaoWebAPI();
    private final BookDaoWebAPI bookDaoWebAPI = new BookDaoWebAPI();

    // getter & setter
    public MainApplication getApp() { return app; }

    public void setWindowTitle() {
        stage.setTitle(WINDOW_TITLE);
    }

    public void setWindowTitle(String title) {
        stage.setTitle(
                String.format("%s — %s", WINDOW_TITLE, title)
        );
    }

    public void setFormatStyle(String filepath) {
        scene.getStylesheets().set(CSS_FORMAT_INDEX, filepath);
    }

    public void setColorTheme(String filepath) {
        scene.getStylesheets().set(CSS_COLOR_INDEX, filepath);
    }

    // constructor
    public MainController(MainApplication app, Stage stage) {
        // view
        root = new MainView(this);

        // scene
        scene = new Scene(root, INITIAL_WIDTH, INITIAL_HEIGHT);
        scene.getStylesheets().add(DEFAULT_FILEPATH_CSS_FORMAT);
        scene.getStylesheets().add(DEFAULT_FILEPATH_CSS_COLOR);

        // stage
        this.app = app;
        this.stage = stage;
        setWindowTitle();
        stage.getIcons().add(new Image(Objects.requireNonNull(
                MainController.class.getResourceAsStream("ook.png")
        )));
        stage.setMinWidth(MINIMAL_WIDTH);
        stage.setMinHeight(MINIMAL_HEIGHT);
        stage.setScene(scene);
        stage.show();
    }

    /*
        EVENT HANDLER
    */
    public void defineSearch(ActionEvent actionEvent) {
        switch (root.getSearchSelector().getValue()) {
            case AUTHOR -> root.getSearchButton().setOnAction(this::searchAuthorsByName);
            case ISBN -> root.getSearchButton().setOnAction(this::getBookByISBN);
            case TITLE -> root.getSearchButton().setOnAction(this::searchBooksByTitle);
        }
    }

    public void setImage(Image image) {
        root.getImageView().setImage(image);
    }

    public void setStatus(String text) {
        root.getStatusBar().setText(text);
    }

    // Author
    public void searchAuthorsByName(ActionEvent actionEvent) {
        String query = root.getSearchField().getText();
        List<Author> authorList = authorDaoWebAPI.read(query);
        root.setAuthorListView(authorList);
    }

    public void pickAuthorFromSearch(MouseEvent mouseEvent) {
        Author author = root.getAuthorListView().getSelectionModel().getSelectedItem();
        authorDaoWebAPI.update(author);
        showAuthorDetails(author);
    }

    public void showAuthorDetails(Author author) {
        resetImageAndStatus();
        root.setAuthorDetailView(author);
    }

    public void showBookDetails(Book book) {
        resetImageAndStatus();
        root.setBookDetailView(book);
    }

    public void resetImageAndStatus() {
        setImage(null);
        setStatus("");
    }

    // Book
    private void getBookByISBN(ActionEvent actionEvent) {
        String input = root.getSearchField().getText();
        Isbn isbn;
        try {
            isbn = new Isbn(input);
            showBookDetails(
                    bookDaoWebAPI.readOne(isbn)
            );
        } catch (IllegalArgumentException e) {
            setStatus(
                    String.format("Not a valid ISBN: %s", input)
            );
        }
    }

    // TODO: Replace dummy implementation
    public void searchBooksByTitle(ActionEvent actionEvent) {
        System.out.printf(
                "Searching for books titled \"%s\".\n",
                root.getSearchField().getText()
        );
    }

}
