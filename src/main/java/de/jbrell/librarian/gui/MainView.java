package de.jbrell.librarian.gui;

import de.jbrell.librarian.models.Author;
import de.jbrell.librarian.models.Book;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

import java.util.List;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 02.12.21
 */
public class MainView extends BorderPane implements View<MainController> {
    // static fields
    public static final String STYLE_ID = "main";

    // instance fields
    private final MainController controller;
    private final TextField searchField = new TextField();
    private final ChoiceBox<SearchObject> searchSelector = new ChoiceBox<>();
    private final Button searchButton  = new Button("Suche");
    private AuthorListView authorListView;
    private AuthorDetailView authorDetailView;
    private BookDetailView bookDetailView;
    private final ImageView imageView = new ImageView();
    private final Label statusBar = new Label(" ");

    // getter
    @Override
    public MainController getController() { return controller; }

    public TextField getSearchField() {
        return searchField;
    }
    public ChoiceBox<SearchObject> getSearchSelector() {
        return searchSelector;
    }
    public Button getSearchButton() {
        return searchButton;
    }

    public AuthorListView getAuthorListView() {
        return authorListView;
    }
    public void setAuthorListView(List<Author> authorList) {
        setLeft(
                authorListView = new AuthorListView(controller, authorList)
        );
    }

    public AuthorDetailView getAuthorDetailView() {
        return authorDetailView;
    }
    public void setAuthorDetailView(Author author) {
        setCenter(
                authorDetailView = new AuthorDetailView(controller, author)
        );
    }

    public BookDetailView getBookDetailView() {
        return bookDetailView;
    }
    public void setBookDetailView(Book book) {
        setCenter(
                bookDetailView = new BookDetailView(controller, book)
        );
    }

    public ImageView getImageView() {
        return imageView;
    }

    public Label getStatusBar() {
        return statusBar;
    }

    // constructor
    public MainView(MainController controller) {
        this.controller = controller;
        setId(STYLE_ID);
        buildTop();
        buildRight();
        buildBottom();
    }

    // build methods
    public void buildTop() {
        searchField.setOnAction(searchButton::fireEvent);
        searchSelector.getItems().addAll(SearchObject.values());
        searchSelector.setValue(SearchObject.AUTHOR);
        searchSelector.setOnAction(controller::defineSearch);
        searchButton.setOnAction(controller::searchAuthorsByName);
        HBox searchBar = new HBox(
                searchField, searchSelector, searchButton
        );
        searchBar.getStyleClass().add("bar");
        searchBar.setId("searchBar");
        searchBar.setAlignment(Pos.CENTER);
        setTop(searchBar);
    }

    public void buildRight() {
        setRight(imageView);
    }

    public void buildBottom() {
        statusBar.getStyleClass().add("bar");
        statusBar.setId("statusBar");
        statusBar.setAlignment(Pos.CENTER_LEFT);
        setBottom(statusBar);
    }

}
