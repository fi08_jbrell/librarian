package de.jbrell.librarian.gui;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 04.12.21
 */
public enum SearchObject {
    // members
    AUTHOR("Autor"),
    ISBN("ISBN"),
    TITLE("Titel");

    // instance fields
    private final String value;

    // constructors
    SearchObject(String value) {
        this.value = value;
    }

    // methods
    @Override
    public String toString() {
        return value;
    }
}
