package de.jbrell.librarian.gui;

import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 03.12.21
 */
public interface View<T> {

    DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(
            "d MMMM yyyy", Locale.ENGLISH
    );

    T getController();
}
