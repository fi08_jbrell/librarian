package de.jbrell.librarian.gui;

import de.jbrell.librarian.models.*;
import javafx.geometry.VPos;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.text.TextAlignment;

import javax.security.auth.Subject;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 09.12.21
 */
public class BookDetailView extends ScrollPane implements View<MainController> {

    // static fields
    public static final String[] STYLE_CLASSES = {"DetailView"};
    public static final String STYLE_ID = "bookDetailView";

    // instance fields
    private final MainController controller;
    private final GridPane gridPane = new GridPane();

    private final Map<String, Label> keyLabels = Map.ofEntries(
            Map.entry("title", new Label("Titel")),
            Map.entry("subtitle", new Label("Untertitel")),
            Map.entry("notes", new Label("Anmerkung")),
            Map.entry("byStatement", new Label("")),
            Map.entry("authors", new Label("Autoren")),
            Map.entry("pagination", new Label("Seitenzählung")),
            Map.entry("pageCount", new Label("Seitenanzahl")),
            Map.entry("publishers", new Label("Herausgeber")),
            Map.entry("publishPlaces", new Label("Verlagsort")),
            Map.entry("publishDate", new Label("Erscheinungsdatum")),
            Map.entry("subjects", new Label("Themen")),
            Map.entry("excerpts", new Label("Auszüge")),
//            Map.entry("cover", new Label("Umschlag")),
//            Map.entry("identifiers", new Label("IDs")),
            Map.entry("links", new Label("Links"))
    );

    {
        for (Label label : keyLabels.values()) {
            label.getStyleClass().addAll("cell", "key");
        }
    }

    private final Map<String, Label> valueLabels = Map.ofEntries(
            Map.entry("title", new Label()),
            Map.entry("subtitle", new Label()),
            Map.entry("notes", new Label()),
            Map.entry("byStatement", new Label()),
            Map.entry("authors", new Label()),
            Map.entry("pagination", new Label()),
            Map.entry("pageCount", new Label()),
            Map.entry("publishers", new Label()),
            Map.entry("publishPlaces", new Label()),
            Map.entry("publishDate", new Label()),
            Map.entry("subjects", new Label()),
            Map.entry("excerpts", new Label()),
//            Map.entry("cover", new Label()),
//            Map.entry("identifiers", new Label()),
            Map.entry("links", new Label())
    );

    {
        for (Label label : valueLabels.values()) {
            label.getStyleClass().addAll("cell", "value");
        }
    }

    // getter
    @Override
    public MainController getController() {
        return controller;
    }

    public GridPane getGridPane() {
        return gridPane;
    }

    // setter
    private void setBook(Book book) {
        if (book != null) {
            build(book);
        }
    }

    // constructor
    public BookDetailView(MainController controller, Book book) {
        // attributes
        this.controller = controller;
        // style
        for (String styleClass : STYLE_CLASSES) {
            getStyleClass().add(styleClass);
        }
        setId(STYLE_ID);
        // format
        setContent(gridPane);
        setFitToWidth(true);
        setPannable(true);
        setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
        setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
        // fill
        setBook(book);
    }

    // build methods
    private void build(Book book) {
        // create and fill rows
        showCover(book);
        buildTitleRow(book);
        buildSubtitleRow(book);
        buildNotesRow(book);
        buildByStatementRow(book);
        buildAuthorsSection(book);
        buildPaginationRow(book);
        buildPageCountRow(book);
        buildPublishersRow(book);
        buildPublishPlacesRow(book);
        buildPublishDateRow(book);
        buildSubjectsSection(book);
        buildExcerptsRow(book);
        buildLinksSection(book);
        buildEmptyRow();
        // align their content
        for (int i = 0; i < gridPane.getRowCount(); i++) {
            RowConstraints rowConstraints = new RowConstraints();
            rowConstraints.setValignment(VPos.TOP);
            gridPane.getRowConstraints().add(rowConstraints);
        }
    }

    private void buildEmptyRow() {
        int rowIndex = gridPane.getRowCount();
        gridPane.add(new Label(), 0, rowIndex, 2, 1);
    }

    private void showCover(Book book) {
        BookCover cover = book.getCover();
        if (cover != null) {
            Image image = book.getCover().getImageLarge();
            if (image == null) {
                image = new Image(
                        cover.getUrlLarge().toString(),
                        true
                );
                cover.setImageLarge(image);
            }
            controller.setImage(image);
        }
    }

    private void buildTitleRow(Book book) {
        String title = book.getTitle();
        if (title != null) {
            Label keyLabel = keyLabels.get("title");
            Label valueLabel = valueLabels.get("title");
            valueLabel.setText(title);
            int rowIndex = gridPane.getRowCount();
            gridPane.add(keyLabel, 0, rowIndex);
            gridPane.add(valueLabel, 1, rowIndex);
        }
    }

    private void buildSubtitleRow(Book book) {
        String subtitle = book.getSubtitle();
        if (subtitle != null) {
            Label keyLabel = keyLabels.get("subtitle");
            Label valueLabel = valueLabels.get("subtitle");
            valueLabel.setText(subtitle);
            int rowIndex = gridPane.getRowCount();
            gridPane.add(keyLabel, 0, rowIndex);
            gridPane.add(valueLabel, 1, rowIndex);
        }
    }

    private void buildNotesRow(Book book) {
        String notes = book.getNotes();
        if (notes != null) {
            Label keyLabel = keyLabels.get("notes");
            Label valueLabel = valueLabels.get("notes");
            valueLabel.setText(notes);
            int rowIndex = gridPane.getRowCount();
            gridPane.add(keyLabel, 0, rowIndex);
            gridPane.add(valueLabel, 1, rowIndex);
        }
    }

    private void buildByStatementRow(Book book) {
        String byStatement = book.getByStatement();
        if (byStatement != null) {
            Label keyLabel = keyLabels.get("byStatement");
            Label valueLabel = valueLabels.get("byStatement");
            valueLabel.setText(byStatement);
            int rowIndex = gridPane.getRowCount();
            gridPane.add(keyLabel, 0, rowIndex);
            gridPane.add(valueLabel, 1, rowIndex);
        }
    }

    private void buildAuthorsSection(Book book) {
        List<Author> authors = book.getAuthors();
        if (authors != null && authors.size() > 0) {
            Label keyLabel = keyLabels.get("authors");
            int rowIndex = gridPane.getRowCount();
            gridPane.add(keyLabel, 0, rowIndex);
            for (Author author : authors) {
                Hyperlink authorLink = new Hyperlink(
                        author.getName()
                );
                authorLink.getStyleClass().addAll("cell", "value");
                authorLink.setOnAction(e ->
                        controller.showAuthorDetails(author)
                );
                gridPane.add(authorLink, 1, rowIndex);
                rowIndex++;
            }
        }
    }

    private void buildPaginationRow(Book book) {
        String pagination = book.getPagination();
        if (pagination != null) {
            Label keyLabel = keyLabels.get("pagination");
            Label valueLabel = valueLabels.get("pagination");
            valueLabel.setText(pagination);
            int rowIndex = gridPane.getRowCount();
            gridPane.add(keyLabel, 0, rowIndex);
            gridPane.add(valueLabel, 1, rowIndex);
        }
    }

    private void buildPageCountRow(Book book) {
        Integer pageCount = book.getPageCount();
        if (pageCount != null) {
            Label keyLabel = keyLabels.get("pageCount");
            Label valueLabel = valueLabels.get("pageCount");
            valueLabel.setText(
                    String.valueOf(pageCount)
            );
            int rowIndex = gridPane.getRowCount();
            gridPane.add(keyLabel, 0, rowIndex);
            gridPane.add(valueLabel, 1, rowIndex);
        }
    }

    private void buildPublishersRow(Book book) {
        List<Publisher> publishers = book.getPublishers();
        if (publishers != null && publishers.size() > 0) {
            Label keyLabel = keyLabels.get("publishers");
            Label valueLabel = valueLabels.get("publishers");
            valueLabel.setText(
                    publishers.stream()
                            .map(Publisher::getName)
                            .collect(Collectors.joining("\n"))
            );
            int rowIndex = gridPane.getRowCount();
            gridPane.add(keyLabel, 0, rowIndex);
            gridPane.add(valueLabel, 1, rowIndex);
        }
    }

    private void buildPublishPlacesRow(Book book) {
        List<String> publishPlaces = book.getPublishPlaces();
        if (publishPlaces != null && publishPlaces.size() > 0) {
            Label keyLabel = keyLabels.get("publishPlaces");
            Label valueLabel = valueLabels.get("publishPlaces");
            valueLabel.setText(
                    String.join("\n", publishPlaces)
            );
            int rowIndex = gridPane.getRowCount();
            gridPane.add(keyLabel, 0, rowIndex);
            gridPane.add(valueLabel, 1, rowIndex);
        }
    }

    private void buildPublishDateRow(Book book) {
        String publishDateString;
        LocalDate publishDate = book.getPublishDate();
        if (publishDate != null) {
            publishDateString = publishDate.format(DATE_FORMATTER);
        } else {
            Integer publishYear = book.getPublishYear();
            if (publishYear != null) {
                publishDateString = String.valueOf(publishYear);
            } else {
                publishDateString = book.getPublishDateString();
            }
        }
        if (publishDateString != null) {
            Label keyLabel = keyLabels.get("publishDate");
            Label valueLabel = valueLabels.get("publishDate");
            valueLabel.setText(publishDateString);
            int rowIndex = gridPane.getRowCount();
            gridPane.add(keyLabel, 0, rowIndex);
            gridPane.add(valueLabel, 1, rowIndex);
        }
    }

    private void buildSubjectsSection(Book book) {
        List<Link> subjects = book.getSubjects();
        if (subjects != null && subjects.size() > 0) {
            Label keyLabel = keyLabels.get("subjects");
            int rowIndex = gridPane.getRowCount();
            gridPane.add(keyLabel, 0, rowIndex);
            for (Link subject : subjects) {
                String url = subject.getUrl().toString();
                Hyperlink subjectLink = new Hyperlink(
                        subject.getTitle()
                );
                subjectLink.getStyleClass().addAll("cell", "value");
                subjectLink.setOnAction(e ->
                        controller.getApp().openBrowser(url)
                );
                gridPane.add(subjectLink, 1, rowIndex);
                rowIndex++;
            }
        }
    }

    private void buildExcerptsRow(Book book) {
        List<BookExcerpt> excerpts = book.getExcerpts();
        if (excerpts != null && excerpts.size() > 0) {
            Label keyLabel = keyLabels.get("excerpts");
            int rowIndex = gridPane.getRowCount();
            gridPane.add(keyLabel, 0, rowIndex, 2, 1);
            for (BookExcerpt excerpt : excerpts) {
                // key
                rowIndex++;
                Label excerptComment = new Label(excerpt.getComment());
                excerptComment.getStyleClass().addAll("cell", "subKey");
                gridPane.add(excerptComment, 0, rowIndex, 2, 1);
                // value
                rowIndex++;
                Label excerptText = new Label(excerpt.getText());
                excerptText.getStyleClass().addAll("cell", "value", "subValue");
                excerptText.setWrapText(true);
                excerptText.setTextAlignment(TextAlignment.JUSTIFY);
                gridPane.add(excerptText, 0, rowIndex, 2, 1);
            }
        }
    }

    private void buildLinksSection(Book book) {
        List<Link> links = book.getLinks();
        if (links != null && links.size() > 0) {
            Label keyLabel = keyLabels.get("links");
            int rowIndex = gridPane.getRowCount();
            gridPane.add(keyLabel, 0, rowIndex, 2, 1);
            for (Link link : links) {
                rowIndex++;
                // key
                Label linkTitle = new Label(link.getTitle());
                linkTitle.getStyleClass().addAll("cell", "subKey");
                gridPane.add(linkTitle, 0, rowIndex);
                // value
                String url = link.getUrl().toString();
                Hyperlink linkAddress = new Hyperlink(url);
                linkAddress.getStyleClass().addAll("cell", "value", "subValue");
                linkAddress.setOnAction(e ->
                        controller.getApp().openBrowser(url)
                );
                gridPane.add(linkAddress, 1, rowIndex);
            }
        }
    }



}


