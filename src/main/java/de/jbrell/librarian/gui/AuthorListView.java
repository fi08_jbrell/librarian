package de.jbrell.librarian.gui;

import de.jbrell.librarian.models.Author;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;

import java.util.List;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 04.12.21
 */
public class AuthorListView extends ListView<Author> implements View<MainController> {
    // static fields

    // instance fields
    private final MainController controller;
    private ObservableList<Author> observableList = FXCollections.observableArrayList();

    // getter & setter
    @Override
    public MainController getController() {
        return controller;
    }

    public void set(List<Author> authorList) {
        setItems(
                observableList = FXCollections.observableArrayList(authorList)
        );
    }

    // constructor
    public AuthorListView(MainController controller, List<Author> authorList) {
        this.controller = controller;
        setCellFactory(new AuthorCellFactory());
        set(authorList);
        setOnMouseClicked(controller::pickAuthorFromSearch);
    }

    // public methods

}
