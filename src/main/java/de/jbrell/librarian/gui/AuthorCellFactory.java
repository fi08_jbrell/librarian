package de.jbrell.librarian.gui;

import de.jbrell.librarian.models.Author;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 04.12.21
 */
public class AuthorCellFactory implements Callback<ListView<Author>, ListCell<Author>> {
    // static fields
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy", Locale.ENGLISH);

    // public methods
    @Override
    public ListCell<Author> call(ListView<Author> authorListView) {
        return new ListCell<>(){
            @Override
            public void updateItem(Author author, boolean empty) {
                super.updateItem(author, empty);
                if (empty || author == null) {
                    setText(null);
                } else {
                    StringBuilder preview = new StringBuilder(author.getName());
//                    String birthDateString;
//                    LocalDate birthDate = author.getBirthDate();
//                    if (birthDate != null) {
//                        birthDateString = birthDate.format(dateTimeFormatter);
//                    } else {
//                        birthDateString = author.getBirthDateString();
//                    }
//                    if (birthDateString != null) {
//                        preview.append(String.format(" (*%s)", birthDateString));
//                    }
                    setText(preview.toString());
                }
            }
        };
    }
}
