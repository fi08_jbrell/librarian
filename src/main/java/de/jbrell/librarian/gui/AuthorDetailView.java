package de.jbrell.librarian.gui;

import de.jbrell.librarian.models.Author;
import de.jbrell.librarian.models.Link;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.text.TextAlignment;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 02.12.21
 */
public class AuthorDetailView extends ScrollPane implements View<MainController> {

    // static fields
    public static final String[] STYLE_CLASSES = {"DetailView"};
    public static final String STYLE_ID = "authorDetailView";

    // instance fields
    private final MainController controller;

    private final GridPane gridPane = new GridPane();
    private static final ColumnConstraints columnConstraintsKeys = new ColumnConstraints();
    private static final ColumnConstraints columnConstraintsValues = new ColumnConstraints();
    {
        columnConstraintsKeys.setPercentWidth(34);
        columnConstraintsValues.setPercentWidth(66);
        gridPane.getColumnConstraints().addAll(columnConstraintsKeys, columnConstraintsValues);
    }

    private final Map<String, Label> keyLabels = Map.ofEntries(
            Map.entry("name", new Label("Name")),
            Map.entry("birth", new Label("Geburt")),
            Map.entry("death", new Label("Tod")),
            Map.entry("title", new Label("Titel")),
            Map.entry("personalName", new Label("Name (persönlich)")),
            Map.entry("alternateNames", new Label("Alternative Namen")),
            Map.entry("bio", new Label("Biografie")),
            Map.entry("workCount", new Label("Anzahl Werke")),
            Map.entry("topWork", new Label("Größtes Werk")),
            Map.entry("topSubjects", new Label("Themen")),
            Map.entry("links", new Label("Links"))
    );
    {
        for (Label label : keyLabels.values()) {
            label.getStyleClass().addAll("cell", "key");
        }
    }
    private final Map<String, Label> valueLabels = Map.ofEntries(
            Map.entry("name", new Label()),
            Map.entry("birth", new Label()),
            Map.entry("death", new Label()),
            Map.entry("title", new Label()),
            Map.entry("personalName", new Label()),
            Map.entry("alternateNames", new Label()),
            Map.entry("bio", new Label()),
            Map.entry("workCount", new Label()),
            Map.entry("topWork", new Label()),
            Map.entry("topSubjects", new Label())
    );
    {
        for (Label label : valueLabels.values()) {
            label.getStyleClass().addAll("cell", "value");
        }
    }

    // getter
    @Override
    public MainController getController() { return controller; }
    public GridPane getGridPane() { return gridPane; }

    // setter
    private void setAuthor(Author author) {
        if (author != null) {
            build(author);
        }
    }

    // constructor
    public AuthorDetailView(MainController controller, Author author) {
        // attributes
        this.controller = controller;
        // style
        for (String styleClass : STYLE_CLASSES) {
            getStyleClass().add(styleClass);
        }
        setId(STYLE_ID);
        // format
        setContent(gridPane);
        setFitToWidth(true);
        setPannable(true);
        setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
        setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
        // fill
        setAuthor(author);
    }

    // build methods
    private void build(Author author) {
        // create and fill rows
        buildNameRow(author);
        buildBirthRow(author);
        buildDeathRow(author);
        buildTitleRow(author);
        buildPersonalNameRow(author);
        buildAlternateNamesRow(author);
        buildWorkCountRow(author);
        buildTopWorkRow(author);
        buildTopSubjectsRow(author);
        buildBioRow(author);
        buildLinksSection(author);
        buildEmptyRow();
        // align their content
        for (int i = 0; i < gridPane.getRowCount(); i++) {
            RowConstraints rowConstraints = new RowConstraints();
            rowConstraints.setValignment(VPos.TOP);
            gridPane.getRowConstraints().add(rowConstraints);
        }
    }

    private void buildEmptyRow() {
        int rowIndex = gridPane.getRowCount();
        gridPane.add(new Label(), 0, rowIndex, 2, 1);
    }

    private void buildNameRow(Author author) {
        String name = author.getName();
        if (name != null) {
            Label keyLabel = keyLabels.get("name");
            Label valueLabel = valueLabels.get("name");
            valueLabel.setText(name);
            int rowIndex = gridPane.getRowCount();
            gridPane.add(keyLabel, 0, rowIndex);
            gridPane.add(valueLabel, 1, rowIndex);
        }
    }

    private void buildBirthRow(Author author) {
        String birthDateString;
        LocalDate birthDate = author.getBirthDate();
        if (birthDate != null) {
            birthDateString = birthDate.format(DATE_FORMATTER);
        } else {
            Integer birthYear = author.getBirthYear();
            if (birthYear != null) {
                birthDateString = String.valueOf(birthYear);
            } else {
                birthDateString = author.getBirthDateString();
            }
        }
        if (birthDateString != null) {
            Label keyLabel = keyLabels.get("birth");
            Label valueLabel = valueLabels.get("birth");
            valueLabel.setText(birthDateString);
            int rowIndex = gridPane.getRowCount();
            gridPane.add(keyLabel, 0, rowIndex);
            gridPane.add(valueLabel, 1, rowIndex);
        }
    }

    private void buildDeathRow(Author author) {
        String deathDateString;
        LocalDate deathDate = author.getDeathDate();
        if (deathDate != null) {
            deathDateString = deathDate.format(DATE_FORMATTER);
        } else {
            Integer deathYear = author.getDeathYear();
            if (deathYear != null) {
                deathDateString = String.valueOf(deathYear);
            } else {
                deathDateString = author.getBirthDateString();
            }
        }
        if (deathDateString != null) {
            Label keyLabel = keyLabels.get("death");
            Label valueLabel = valueLabels.get("death");
            valueLabel.setText(deathDateString);
            int rowIndex = gridPane.getRowCount();
            gridPane.add(keyLabel, 0, rowIndex);
            gridPane.add(valueLabel, 1, rowIndex);
        }
    }

    private void buildTitleRow(Author author) {
        String title = author.getTitle();
        if (title != null) {
            Label keyLabel = keyLabels.get("title");
            Label valueLabel = valueLabels.get("title");
            valueLabel.setText(title);
            int rowIndex = gridPane.getRowCount();
            gridPane.add(keyLabel, 0, rowIndex);
            gridPane.add(valueLabel, 1, rowIndex);
        }
    }

    private void buildPersonalNameRow(Author author) {
        String personalName = author.getPersonalName();
        if (personalName != null) {
            Label keyLabel = keyLabels.get("personalName");
            Label valueLabel = valueLabels.get("personalName");
            valueLabel.setText(personalName);
            int rowIndex = gridPane.getRowCount();
            gridPane.add(keyLabel, 0, rowIndex);
            gridPane.add(valueLabel, 1, rowIndex);
        }
    }

    private void buildAlternateNamesRow(Author author) {
        List<String> alternateNames = author.getAlternateNames();
        if (alternateNames != null && alternateNames.size() > 0) {
            Label keyLabel = keyLabels.get("alternateNames");
            Label valueLabel = valueLabels.get("alternateNames");
            valueLabel.setText(
                    String.join("\n", alternateNames)
            );
            int rowIndex = gridPane.getRowCount();
            gridPane.add(keyLabel, 0, rowIndex);
            gridPane.add(valueLabel, 1, rowIndex);
        }
    }

    private void buildWorkCountRow(Author author) {
        Integer workCount = author.getWorkCount();
        if (workCount != null) {
            Label keyLabel = keyLabels.get("workCount");
            Label valueLabel = valueLabels.get("workCount");
            valueLabel.setText(String.valueOf(workCount));
            int rowIndex = gridPane.getRowCount();
            gridPane.add(keyLabel, 0, rowIndex);
            gridPane.add(valueLabel, 1, rowIndex);
        }
    }

    private void buildTopWorkRow(Author author) {
        String topWork = author.getTopWork();
        if (topWork != null) {
            Label keyLabel = keyLabels.get("topWork");
            Label valueLabel = valueLabels.get("topWork");
            valueLabel.setText(topWork);
            int rowIndex = gridPane.getRowCount();
            gridPane.add(keyLabel, 0, rowIndex);
            gridPane.add(valueLabel, 1, rowIndex);
        }
    }

    private void buildTopSubjectsRow(Author author) {
        List<String> topSubjects = author.getTopSubjects();
        if (topSubjects != null && topSubjects.size() > 0) {
            Label keyLabel = keyLabels.get("topSubjects");
            Label valueLabel = valueLabels.get("topSubjects");
            valueLabel.setText(
                    String.join("\n", topSubjects)
            );
            int rowIndex = gridPane.getRowCount();
            gridPane.add(keyLabel, 0, rowIndex);
            gridPane.add(valueLabel, 1, rowIndex);
        }
    }

    private void buildBioRow(Author author) {
        String bio = author.getBio();
        if (bio != null) {
            Label keyLabel = keyLabels.get("bio");
            Label valueLabel = valueLabels.get("bio");
            valueLabel.getStyleClass().add("subValue");
            valueLabel.setText(bio);
            valueLabel.setWrapText(true);
            valueLabel.setTextAlignment(TextAlignment.JUSTIFY);
            int rowIndex = gridPane.getRowCount();
            gridPane.add(keyLabel, 0, rowIndex, 2, 1);
            gridPane.add(valueLabel, 0, rowIndex+1, 2, 1);
        }
    }

    private void buildLinksSection(Author author) {
        List<Link> links = author.getLinks();
        if (links != null && links.size() > 0) {
            Label keyLabel = keyLabels.get("links");
            int rowIndex = gridPane.getRowCount();
            gridPane.add(keyLabel, 0, rowIndex, 2, 1);
            for (Link link : links) {
                rowIndex++;
                // key
                Label linkTitle = new Label(link.getTitle());
                linkTitle.getStyleClass().addAll("cell", "subKey");
                gridPane.add(linkTitle, 0, rowIndex);
                // value
                String url = link.getUrl().toString();
                Hyperlink linkAddress = new Hyperlink(url);
                linkAddress.getStyleClass().addAll("cell", "value", "subValue");
                linkAddress.setOnAction(e ->
                        controller.getApp().openBrowser(url)
                );
                gridPane.add(linkAddress, 1, rowIndex);
            }
        }
    }

}
