package de.jbrell.librarian.daos;

import de.jbrell.librarian.models.*;
import de.jbrell.librarian.webservice.Isbn;
import de.jbrell.librarian.webservice.OpenLibraryService;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 08.12.21
 */
public class BookDaoWebAPI implements Dao<Book> {

    // static fields
    private static final Pattern PATTERN_GET_AUTHOR_FROM_URL = Pattern.compile(
            ".*/authors/(OL\\d+A)/.*",
            Pattern.CASE_INSENSITIVE
    );
    private static final Map<String, Book> cacheByOpenLibraryKey = new HashMap<>();
    private static final Map<Isbn, Book> cacheByISBN = new HashMap<>();

    // static methods
    public static String getAuthorKeyFromUrl(String url) {
        String openLibraryKey = "";
        Matcher matcher = PATTERN_GET_AUTHOR_FROM_URL.matcher(url);
        if (matcher.matches()) {
            openLibraryKey = matcher.group(1);
        }
        return openLibraryKey;
    }

    // methods: JSON → Java
    public static void updateTitleFromJson(JSONObject source, Book target) {
        String title = source.optString("title");
        if (!title.isEmpty()) {
            target.setTitle(title);
        }
    }

    public static void updateSubtitleFromJson(JSONObject source, Book target) {
        String subtitle = source.optString("subtitle");
        if (!subtitle.isEmpty()) {
            target.setSubtitle(subtitle);
        }
    }

    public static void updateAuthorsFromJson(JSONObject source, Book target) {
        JSONArray authorsJSONArray = source.optJSONArray("authors");
        if (authorsJSONArray == null) {
            return;
        }
        int authorsArrayLength = authorsJSONArray.length();
        if (authorsArrayLength <= 0) {
            return;
        }
        List<Author> authorList = new ArrayList<>(authorsArrayLength);
        for (int i = 0; i < authorsArrayLength; i++) {
            JSONObject authorJSONObject = authorsJSONArray.getJSONObject(i);
            String openLibraryKey = getAuthorKeyFromUrl(
                    authorJSONObject.getString("url")
            );
            if (openLibraryKey.isEmpty()) {
                continue;
            }
            AuthorDaoWebAPI authorDaoWebAPI = new AuthorDaoWebAPI();
            Author author = authorDaoWebAPI.readOne(openLibraryKey);
            authorList.add(author);
        }
        if (authorList.size() > 0) {
            if (target.getAuthors() == null) {
                target.setAuthors(authorList);
            } else {
                target.getAuthors().addAll(authorList);
            }
        }
    }

    public static void updateIdentifiersFromJson(JSONObject source, Book target) {
        JSONObject identifiersJSONObject = source.optJSONObject("identifiers");
        if (identifiersJSONObject == null) {
            return;
        }
        BookIdentifiers bookIdentifiers = target.getIdentifiers();
        if (bookIdentifiers == null) {
            bookIdentifiers = new BookIdentifiers();
            target.setIdentifiers(bookIdentifiers);
        }
        if (identifiersJSONObject.has("isbn10")) {
            JSONArray identifierJSONArray = identifiersJSONObject.getJSONArray("isbn10");
            bookIdentifiers.setIsbn10(identifierJSONArray.getString(0));
        }
        if (identifiersJSONObject.has("isbn13")) {
            JSONArray identifierJSONArray = identifiersJSONObject.getJSONArray("isbn13");
            bookIdentifiers.setIsbn13(identifierJSONArray.getString(0));
        }
        if (identifiersJSONObject.has("amazon")) {
            JSONArray identifierJSONArray = identifiersJSONObject.getJSONArray("amazon");
            bookIdentifiers.setAmazon(identifierJSONArray.getString(0));
        }
        if (identifiersJSONObject.has("goodreads")) {
            JSONArray identifierJSONArray = identifiersJSONObject.getJSONArray("goodreads");
            bookIdentifiers.setGoodreads(identifierJSONArray.getString(0));
        }
        if (identifiersJSONObject.has("google")) {
            JSONArray identifierJSONArray = identifiersJSONObject.getJSONArray("google");
            bookIdentifiers.setGoogle(identifierJSONArray.getString(0));
        }
        if (identifiersJSONObject.has("lccn")) {
            JSONArray identifierJSONArray = identifiersJSONObject.getJSONArray("lccn");
            bookIdentifiers.setLccn(identifierJSONArray.getString(0));
        }
        if (identifiersJSONObject.has("librarything")) {
            JSONArray identifierJSONArray = identifiersJSONObject.getJSONArray("librarything");
            bookIdentifiers.setLibrarything(identifierJSONArray.getString(0));
        }
        if (identifiersJSONObject.has("oclc")) {
            JSONArray identifierJSONArray = identifiersJSONObject.getJSONArray("oclc");
            bookIdentifiers.setOclc(identifierJSONArray.getString(0));
        }
        if (identifiersJSONObject.has("openlibrary")) {
            JSONArray identifierJSONArray = identifiersJSONObject.getJSONArray("openlibrary");
            bookIdentifiers.setOpenlibrary(identifierJSONArray.getString(0));
        }
        if (identifiersJSONObject.has("projectGutenberg")) {
            JSONArray identifierJSONArray = identifiersJSONObject.getJSONArray("projectGutenberg");
            bookIdentifiers.setProjectGutenberg(identifierJSONArray.getString(0));
        }
    }

    public static void updateSubjectsFromJson(JSONObject source, Book target) {
        JSONArray subjectsJSONArray = source.optJSONArray("subjects");
        if (subjectsJSONArray == null) {
            return;
        }
        int subjectsArrayLength = subjectsJSONArray.length();
        if (subjectsArrayLength <= 0) {
            return;
        }
        List<Link> subjectsList = new ArrayList<>(subjectsArrayLength);
        for (int i = 0; i < subjectsArrayLength; i++) {
            JSONObject subjectJSONObject = subjectsJSONArray.getJSONObject(i);
            try {
                String subjectName = subjectJSONObject.getString("name");
                URL linkUrl = new URL(
                        subjectJSONObject.getString("url")
                );
                subjectsList.add(
                        new Link(subjectName, linkUrl)
                );
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        if (subjectsList.size() > 0) {
            if (target.getSubjects() == null) {
                target.setSubjects(subjectsList);
            } else {
                target.getSubjects().addAll(subjectsList);
            }
        }
    }

    public static void updatePublishersFromJson(JSONObject source, Book target) {
        JSONArray publishersJSONArray = source.optJSONArray("publishers");
        if (publishersJSONArray == null) {
            return;
        }
        int publishersArrayLength = publishersJSONArray.length();
        if (publishersArrayLength <= 0) {
            return;
        }
        List<Publisher> publishersList = new ArrayList<>(publishersArrayLength);
        for (int i = 0; i < publishersArrayLength; i++) {
            JSONObject publisherJSONObject = publishersJSONArray.getJSONObject(i);
            publishersList.add(
                    new Publisher(
                            publisherJSONObject.getString("name")
                    )
            );
        }
        if (publishersList.size() > 0) {
            if (target.getPublishers() == null) {
                target.setPublishers(publishersList);
            } else {
                target.getPublishers().addAll(publishersList);
            }
        }
    }

    public static void updatePublishPlacesFromJson(JSONObject source, Book target) {
        JSONArray publishPlacesJSONArray = source.optJSONArray("publish_places");
        if (publishPlacesJSONArray == null) {
            return;
        }
        int publishPlacesArrayLength = publishPlacesJSONArray.length();
        if (publishPlacesArrayLength <= 0) {
            return;
        }
        List<String> publishPlacesList = new ArrayList<>(publishPlacesArrayLength);
        for (int i = 0; i < publishPlacesArrayLength; i++) {
            JSONObject publishPlacesJSONObject = publishPlacesJSONArray.getJSONObject(i);
            publishPlacesList.add(
                    publishPlacesJSONObject.getString("name")
            );
        }
        if (publishPlacesList.size() > 0) {
            if (target.getPublishPlaces() == null) {
                target.setPublishPlaces(publishPlacesList);
            } else {
                target.getPublishPlaces().addAll(publishPlacesList);
            }
        }
    }

    public static void updatePublishDateFromJson(JSONObject source, Book target) {
        String publishDateString = source.optString("publish_date");
        if (publishDateString.matches("\\d{4}")) {
            target.setPublishYear(Integer.parseInt(publishDateString));
            return;
        }
        if (!publishDateString.isEmpty()) {
            DateTimeParseException finalException = null; // for debugging
            for (DateTimeFormatter dateParseFormatter : OpenLibraryService.DateParseFormatters) {
                try {
                    LocalDate publishDate = LocalDate.parse(publishDateString, dateParseFormatter);
                    target.setPublishDate(publishDate);
                    finalException = null;
                    break;
                } catch (DateTimeParseException exception) {
                    finalException = exception;
                }
            }
            if (finalException != null) {
                finalException.printStackTrace();
            }
            if (target.getPublishDate() == null) {
                target.setPublishDateString(publishDateString);
            }
        }
    }

    public static void updateExcerptsFromJson(JSONObject source, Book target) {
        JSONArray excerptsJSONArray = source.optJSONArray("excerpts");
        if (excerptsJSONArray == null) {
            return;
        }
        int excerptsArrayLength = excerptsJSONArray.length();
        if (excerptsArrayLength <= 0) {
            return;
        }
        List<BookExcerpt> excerptsList = new ArrayList<>(excerptsArrayLength);
        for (int i = 0; i < excerptsArrayLength; i++) {
            JSONObject excerptJSONObject = excerptsJSONArray.getJSONObject(i);
            excerptsList.add(
                    new BookExcerpt(
                            excerptJSONObject.getString("comment"),
                            excerptJSONObject.getString("text")
                    )
            );
        }
        if (excerptsList.size() > 0) {
            if (target.getExcerpts() == null) {
                target.setExcerpts(excerptsList);
            } else {
                target.getExcerpts().addAll(excerptsList);
            }
        }
    }

    public static void updateLinksFromJson(JSONObject source, Book target) {
        JSONArray linksJSONArray = source.optJSONArray("links");
        if (linksJSONArray != null) {
            int linksArrayLength = linksJSONArray.length();
            if (linksArrayLength > 0) {
                List<Link> links = new ArrayList<>(linksArrayLength);
                for (int i = 0; i < linksArrayLength; i++) {
                    JSONObject linkJSONObject = linksJSONArray.getJSONObject(i);
                    try {
                        String linkTitle = linkJSONObject.getString("title");
                        URL linkUrl = new URL(
                                linkJSONObject.getString("url")
                        );
                        links.add(
                                new Link(linkTitle, linkUrl)
                        );
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                }
                if (links.size() > 0) {
                    if (target.getLinks() == null) {
                        target.setLinks(links);
                    } else {
                        target.getLinks().addAll(links);
                    }
                }
            }
        }
    }

    public static void updateCoverFromJson(JSONObject source, Book target) {
        JSONObject coverJSONObject = source.optJSONObject("cover");
        if (coverJSONObject == null) {
            return;
        }
        BookCover bookCover = target.getCover();
        if (bookCover == null) {
            bookCover = new BookCover();
            target.setCover(bookCover);
        }
        if (coverJSONObject.has("small")) {
            try {
                bookCover.setUrlSmall(
                        new URL(
                                coverJSONObject.getString("small")
                        )
                );
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        if (coverJSONObject.has("medium")) {
            try {
                bookCover.setUrlMedium(
                        new URL(
                                coverJSONObject.getString("medium")
                        )
                );
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        if (coverJSONObject.has("large")) {
            try {
                bookCover.setUrlLarge(
                        new URL(
                                coverJSONObject.getString("large")
                        )
                );
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void updatePaginationFromJson(JSONObject source, Book target) {
        String pagination = source.optString("pagination");
        if (!pagination.isEmpty()) {
            target.setPagination(pagination);
        }
    }

    public static void updatePageCountFromJson(JSONObject source, Book target) {
        String pageCount = source.optString("number_of_pages");
        if (!pageCount.isEmpty()) {
            target.setPageCount(Integer.parseInt(pageCount));
        }
    }

    public static void updateNotesFromJson(JSONObject source, Book target) {
        String notes = source.optString("notes");
        if (!notes.isEmpty()) {
            target.setNotes(notes);
        }
    }

    public static void updateByStatementFromJson(JSONObject source, Book target) {
        String byStatement = source.optString("by_statement");
        if (!byStatement.isEmpty()) {
            target.setByStatement(byStatement);
        }
    }

    public static void updateBookFromDetailedJson(JSONObject jsonObject, Book book) {
        // update fields in book object
        updateTitleFromJson(jsonObject, book);
        updateSubtitleFromJson(jsonObject, book);
        updateAuthorsFromJson(jsonObject, book);
        updateIdentifiersFromJson(jsonObject, book);
        updateSubjectsFromJson(jsonObject, book);
        updatePublishersFromJson(jsonObject, book);
        updatePublishPlacesFromJson(jsonObject, book);
        updatePublishDateFromJson(jsonObject, book);
        updateExcerptsFromJson(jsonObject, book);
        updateLinksFromJson(jsonObject, book);
        updateCoverFromJson(jsonObject, book);
        updatePaginationFromJson(jsonObject, book);
        updatePageCountFromJson(jsonObject, book);
        updateNotesFromJson(jsonObject, book);
        updateByStatementFromJson(jsonObject, book);
        // store in cache
        String openLibraryKey = book.getIdentifiers().getOpenlibrary();
        if (openLibraryKey != null && !openLibraryKey.isEmpty()) {
            cacheByOpenLibraryKey.put(openLibraryKey, book);
        }
        String isbn10 = book.getIdentifiers().getIsbn10();
        if (isbn10 != null && !isbn10.isEmpty()) {
            cacheByISBN.put(new Isbn(isbn10), book);
        }
        String isbn13 = book.getIdentifiers().getIsbn13();
        if (isbn13 != null && !isbn13.isEmpty()) {
            cacheByISBN.put(new Isbn(isbn13), book);
        }
    }

    /*
        CRUD
    */
    @Override
    public void create(Book book) {

    }

    @Override
    public List<Book> read(String query) {
        return null;
    }

    public Book readOne(String openLibraryKey) {
        Book book;
        if (cacheByOpenLibraryKey.containsKey(openLibraryKey)) {
            book = cacheByOpenLibraryKey.get(openLibraryKey);
        } else {
            book = new Book();
            BookIdentifiers bookIdentifiers = new BookIdentifiers();
            bookIdentifiers.setOpenlibrary(openLibraryKey);
            book.setIdentifiers(bookIdentifiers);
            update(book);
        }
        return book;
    }

    public Book readOne(Isbn isbn) {
        Book book = cacheByISBN.getOrDefault(isbn, null);
        if (book == null) {
            book = new Book();
            BookIdentifiers bookIdentifiers = new BookIdentifiers();
            if (isbn.isISBN13()) {
                bookIdentifiers.setIsbn13(isbn.isbn());
            } else if (isbn.isISBN10()) {
                bookIdentifiers.setIsbn10(isbn.isbn());
            } else {
                assert false : "Isbn record class is broken.";
            }
            book.setIdentifiers(bookIdentifiers);
            update(book);
        }
        return book;
    }

    @Override
    public void update(Book book) {
        String openLibraryKey = book.getIdentifiers().getOpenlibrary();
        if (openLibraryKey != null && !openLibraryKey.isEmpty()) {
            if (cacheByOpenLibraryKey.containsKey(openLibraryKey)) {
                return;
            }
            JSONObject jsonObject = OpenLibraryService.getBookByOpenLibraryKey(openLibraryKey);
            assert !jsonObject.optString("error").equals("notfound") : String.format(
                    "OpenLibraryKey not found: %s", openLibraryKey
            );
            updateBookFromDetailedJson(jsonObject, book);
            return;
        }
        String isbn13 = book.getIdentifiers().getIsbn13();
        String isbn10 = book.getIdentifiers().getIsbn13();
        Isbn isbn;
        if (isbn13 != null && !isbn13.isEmpty()) {
            isbn = new Isbn(isbn13);
        } else if (isbn10 != null && !isbn10.isEmpty()) {
            isbn = new Isbn(isbn10);
        } else {
            // bail if no isbn was found to search for...
            return;
        }
        if (cacheByISBN.containsKey(isbn)) {
            // ...or if data is already present.
            return;
        }
        JSONObject jsonObject = OpenLibraryService.getBookByISBN(isbn);
        assert !jsonObject.optString("error").equals("notfound") : String.format(
                "ISBN not found: %s", isbn.isbn()
        );
        updateBookFromDetailedJson(jsonObject, book);
    }

    @Override
    public void delete(Book book) {

    }

}
