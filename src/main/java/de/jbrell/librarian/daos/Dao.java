package de.jbrell.librarian.daos;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 01.12.21
 */
public interface Dao<T> {

    // static fields
    DateTimeFormatter dateWriteFormatter = DateTimeFormatter.ofPattern(
            "d MMMM yyyy", Locale.ENGLISH
    );

    // CRUD
    void create(T t);
    List<T> read(String query);
    void update(T t);
    void delete(T t);
}
