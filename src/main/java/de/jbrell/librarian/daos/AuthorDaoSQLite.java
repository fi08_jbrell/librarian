package de.jbrell.librarian.daos;

import de.jbrell.librarian.MainApplication;
import de.jbrell.librarian.db.SQLiteConnector;
import de.jbrell.librarian.models.Author;

import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 06.12.2021
 */
public class AuthorDaoSQLite implements Dao<Author> {
    // instance fields
    private final SQLiteConnector connector = new SQLiteConnector();

    // constructor

    // public methods
    public boolean createTable() {
        String sql = """
                CREATE TABLE IF NOT EXISTS "authors" (
                    "id_author" INTEGER NOT NULL PRIMARY KEY,
                    "name" TEXT NOT NULL,
                    "personal_name" TEXT NULL,
                    "title" TEXT NULL,
                    "birth_date" TEXT NULL,
                    "bio" TEXT NULL,
                    "work_count" INTEGER NULL,
                    "top_work" TEXT NULL,
                    "ol_key" TEXT NOT NULL UNIQUE
                );
                CREATE TABLE IF NOT EXISTS "_alternate_names" (
                    "id_alternate_name" INTEGER NOT NULL PRIMARY KEY,
                    "alternate_name" TEXT NOT NULL,
                    "author_id" INTEGER REFERENCES "authors"(id_author) ON UPDATE CASCADE
                );
                CREATE TABLE IF NOT EXISTS "_top_subjects" (
                    "id_top_subjects" INTEGER NOT NULL PRIMARY KEY,
                    "top_subject" TEXT NOT NULL,
                    "author_id" INTEGER REFERENCES "authors"(id_author) ON UPDATE CASCADE
                );
                """;
        try (connector) {
            Connection connection = connector.getConnection();
            Statement statement = connection.createStatement();
            return statement.execute(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    // CRUD
    @Override
    public void create(Author author) {

    }

    @Override
    public List<Author> read(String query) {
        return null;
    }

    @Override
    public void update(Author author) {

    }

    @Override
    public void delete(Author author) {

    }
}
