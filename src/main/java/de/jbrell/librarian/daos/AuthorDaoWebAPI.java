package de.jbrell.librarian.daos;

import de.jbrell.librarian.models.Author;
import de.jbrell.librarian.models.Link;
import de.jbrell.librarian.webservice.OpenLibraryService;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;

/**
 * @author J.Brell
 * @version 0.1.0
 * @since 01.12.21
 */
public class AuthorDaoWebAPI implements Dao<Author> {

    // instance fields
    private static final Map<String, Author> cachePreviewed = new HashMap<>();
    private static final Map<String, Author> cacheDetailed = new HashMap<>();

    // methods: JSON → Java
    public static void updateNameFromJson(JSONObject source, Author target) {
        String name = source.optString("name");
        if (!name.isEmpty()) {
            target.setName(name);
        }
    }

    public static void updatePersonalNameFromJson(JSONObject source, Author target) {
        String personalName = source.optString("personal_name");
        if (!personalName.isEmpty()) {
            target.setPersonalName(personalName);
        }
    }

    public static void updateAlternateNamesFromJson(JSONObject source, Author target) {
        JSONArray altNamesJSONArray = source.optJSONArray("alternate_names");
        if (altNamesJSONArray != null) {
            int altNamesArrayLength = altNamesJSONArray.length();
            if (altNamesArrayLength > 0) {
                List<String> alternateNames = new ArrayList<>(altNamesArrayLength);
                for (int i = 0; i < altNamesArrayLength; i++) {
                    alternateNames.add(altNamesJSONArray.getString(i));
                }
                if (target.getAlternateNames() == null) {
                    target.setAlternateNames(alternateNames);
                } else {
                    target.getAlternateNames().addAll(alternateNames);
                }
            }
        }
    }

    public static void updateTitleFromJson(JSONObject source, Author target) {
        String title = source.optString("title");
        if (!title.isEmpty()) {
            target.setTitle(title);
        }
    }

    public static void updateBirthDateFromJson(JSONObject source, Author target) {
        String birthDateString = source.optString("birth_date");
        if (birthDateString.matches("\\d{4}")) {
            target.setBirthYear(Integer.parseInt(birthDateString));
            return;
        }
        if (!birthDateString.isEmpty()) {
            DateTimeParseException finalException = null; // for debugging
            for (DateTimeFormatter dateParseFormatter : OpenLibraryService.DateParseFormatters) {
                try {
                    LocalDate birthDate = LocalDate.parse(birthDateString, dateParseFormatter);
                    target.setBirthDate(birthDate);
                    finalException = null;
                    break;
                } catch (DateTimeParseException exception) {
                    finalException = exception;
                }
            }
            if (finalException != null) {
                finalException.printStackTrace();
            }
            if (target.getBirthDate() == null) {
                target.setBirthDateString(birthDateString);
            }
        }
    }

    public static void updateDeathDateFromJson(JSONObject source, Author target) {
        String deathDateString = source.optString("death_date");
        if (deathDateString.matches("\\d{4}")) {
            target.setDeathYear(Integer.parseInt(deathDateString));
            return;
        }
        if (!deathDateString.isEmpty()) {
            DateTimeParseException finalException = null; // for debugging
            for (DateTimeFormatter dateParseFormatter : OpenLibraryService.DateParseFormatters) {
                try {
                    LocalDate deathDate = LocalDate.parse(deathDateString, dateParseFormatter);
                    target.setDeathDate(deathDate);
                    finalException = null;
                    break;
                } catch (DateTimeParseException exception) {
                    finalException = exception;
                }
            }
            if (finalException != null) {
                finalException.printStackTrace();
            }
            if (target.getDeathDate() == null) {
                target.setDeathDateString(deathDateString);
            }
        }
    }

    public static void updateBioFromJson(JSONObject source, Author target) {
        String bio = source.optString("bio");
        if (!bio.isEmpty()) {
            target.setBio(bio);
        }
    }

    public static void updateWorkCountFromJson(JSONObject source, Author target){
        String workCount = source.optString("work_count");
        if (!workCount.isEmpty()) {
            target.setWorkCount(Integer.parseInt(workCount));
        }
    }

    public static void updateTopWorkFromJson(JSONObject source, Author target){
        String topWork = source.optString("top_work");
        if (!topWork.isEmpty()) {
            target.setTopWork(topWork);
        }
    }

    public static void updateTopSubjects(JSONObject source, Author target) {
        JSONArray topSubjectsJSONArray = source.optJSONArray("top_subjects");
        if (topSubjectsJSONArray != null) {
            int topSubjectsArrayLength = topSubjectsJSONArray.length();
            if (topSubjectsArrayLength > 0) {
                List<String> topSubjects = new ArrayList<>(topSubjectsArrayLength);
                for (int i = 0; i < topSubjectsArrayLength; i++) {
                    topSubjects.add(topSubjectsJSONArray.getString(i));
                }
                if (target.getTopSubjects() == null) {
                    target.setTopSubjects(topSubjects);
                } else {
                    target.getTopSubjects().addAll(topSubjects);
                }
            }
        }
    }

    public static void updateLinksFromJson(JSONObject source, Author target) {
        JSONArray linksJSONArray = source.optJSONArray("links");
        if (linksJSONArray != null) {
            int linksArrayLength = linksJSONArray.length();
            if (linksArrayLength > 0) {
                List<Link> links = new ArrayList<>(linksArrayLength);
                for (int i = 0; i < linksArrayLength; i++) {
                    JSONObject linkJSONObject = linksJSONArray.getJSONObject(i);
                    try {
                        String linkTitle = linkJSONObject.getString("title");
                        URL linkUrl = new URL(
                                linkJSONObject.getString("url")
                        );
                        links.add(
                                new Link(linkTitle, linkUrl)
                        );
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                }
                if (links.size() > 0) {
                    if (target.getLinks() == null) {
                        target.setLinks(links);
                    } else {
                        target.getLinks().addAll(links);
                    }
                }
            }
        }
    }

    public static void updateAuthorFromDetailedJson(JSONObject jsonObject, Author author) {
        String openLibraryKey = author.getOpenLibraryKey();
        assert openLibraryKey != null && !openLibraryKey.isEmpty();
        updateNameFromJson(jsonObject, author);
        updatePersonalNameFromJson(jsonObject, author);
        updateAlternateNamesFromJson(jsonObject, author);
        updateTitleFromJson(jsonObject, author);
        updateBirthDateFromJson(jsonObject, author);
        updateDeathDateFromJson(jsonObject, author);
        updateBioFromJson(jsonObject, author);
        updateLinksFromJson(jsonObject, author);
        cacheDetailed.put(openLibraryKey, author);
        cachePreviewed.remove(openLibraryKey);
    }

    public static Optional<Author> createAuthorFromPreviewJson(JSONObject jsonObject) {
        String openLibraryKey = jsonObject.optString("key");
        if (openLibraryKey.isEmpty()) {
            return Optional.empty();
        }
        Author author = new Author();
        author.setOpenLibraryKey(openLibraryKey);
        updateNameFromJson(jsonObject, author);
        updateWorkCountFromJson(jsonObject, author);
        updateTopWorkFromJson(jsonObject, author);
        updateTopSubjects(jsonObject, author);
        cachePreviewed.put(openLibraryKey, author);
        return Optional.of(author);
    }

    /*
        CRUD
    */
    @Override
    public void create(Author author) {
        throw new UnsupportedOperationException("WebAPI is read-only");
    }

    @Override
    public List<Author> read(String query) {
        ArrayList<Author> result = new ArrayList<>();
        JSONObject response = OpenLibraryService.getAuthorsByQuery(query);
        JSONArray entries = response.optJSONArray("docs");
        if (entries == null) {
            return result;
        }
        for (int i = 0; i < entries.length(); i++) {
            JSONObject entry = entries.getJSONObject(i);
            String openLibraryKey = entry.optString("key");
            if (openLibraryKey.isEmpty()) {
                continue;
            }
            if (cacheDetailed.containsKey(openLibraryKey)) {
                result.add(cacheDetailed.get(openLibraryKey));
                continue;
            }
            if (cachePreviewed.containsKey(openLibraryKey)) {
                result.add(cachePreviewed.get(openLibraryKey));
                continue;
            }
            Optional<Author> maybeAuthor = createAuthorFromPreviewJson(entry);
            if (maybeAuthor.isPresent()) {
                Author author = maybeAuthor.get();
                cachePreviewed.put(openLibraryKey, author);
                result.add(author);
            }
        }
        return result;
    }

    public Author readOne(String openLibraryKey) {
        Author author;
        if (cacheDetailed.containsKey(openLibraryKey)) {
            author = cacheDetailed.get(openLibraryKey);
        } else if (cachePreviewed.containsKey(openLibraryKey)) {
            author = cachePreviewed.get(openLibraryKey);
            update(author);
        } else {
            author = new Author();
            author.setOpenLibraryKey(openLibraryKey);
            update(author);
        }
        return author;
    }

    @Override
    public void update(Author author) {
        String openLibraryKey = author.getOpenLibraryKey();
        assert openLibraryKey != null && !openLibraryKey.isEmpty();
        if (cacheDetailed.containsKey(openLibraryKey)) {
            return;
        }
        JSONObject jsonObject = OpenLibraryService.getAuthorByKey(openLibraryKey);
        assert !jsonObject.optString("error").equals("notfound");
        updateAuthorFromDetailedJson(jsonObject, author);
    }

    @Override
    public void delete(Author author) {
        throw new UnsupportedOperationException("WebAPI is read-only");
    }

}
