## FIAE08 - Java SE - The Project

Fällig am 9. Dezember 2021 um 23:59 Uhr.

### Anweisungen

#### 1. Thema

Erstellung einer grafischen Benutzerschnittstelle zur Nutzung eines Webservice.\
Der zugrundeliegende Webservice kann frei und eigenverantwortlich gewählt werden.

#### 2. Anforderungen

- Die grafische Benutzeroberfläche soll mit dem JavaFX Framework erstellt werden.
- Der Webservice muss Daten im JSON-Format liefern.
- Das UI soll nur einen frei wählbaren Teil der API abbilden.
- Die Clean Code Prinzipien sind einzuhalten

#### 3. Optionale Anforderungen

- Zugriff auf den Webservice über das DAO Pattern
- Architektur nach dem MVC Pattern

#### 4. Projektabgabe

Die Bearbeitungszeit des Projektes endet am Do. 09.12.2021 um 23:59 Uhr.\
Abzugeben ist das komplette Projektverzeichnis
(als Link auf ein git Repository oder als Archiv).

#### 5. Präsentation

Am Ende des Projektzeitraumes (Fr. 10.12.2021) ist das Projekt in einer maximal
15 minütigen Präsentation vorzustellen.
Dabei sollen folgende Punkte berücksichtigt werden:
1. Demonstration der Programmfunktion
2. Technische Umsetzung (Quelltext)
3. Beantwortung von Fragen
4. Persönliches Fazit

